from yall.lang.functions import format_yall
from yall.lang.runtime import Runtime
from prompt_toolkit import prompt, PromptSession
from prompt_toolkit.completion import WordCompleter
import traceback

# syntax highlighting https://pygments.org/docs/lexerdevelopment/

def run_repl(vi_mode, files, stdlibs):
    repl = Repl(vi_mode, files, stdlibs)
    repl.run()

class Repl():

    def __init__(self, vi_mode=False, files=None, stdlibs=True):
        self.vi_mode = vi_mode
        self.files = files or []
        self.runtime = Runtime(native_stdlib=stdlibs)
        self.init_prompt_session()
        for f in self.files:
            self.runtime.use_file(f)
        self.update_autocomplete()

    def init_prompt_session(self):
        self.session = PromptSession()
        self.complete = WordCompleter(list(self.runtime.env.keys()))

    def run(self):
        while True:
            try:
                code = self.read_input()
            except KeyboardInterrupt:
                continue
            except EOFError:
                break
            self.evaluate(code)

    def read_input(self):
        lines = []
        while not lines or lines[-1].endswith("\\"):
            line = self.session.prompt(
                "> ",
                vi_mode=self.vi_mode,
                completer=self.complete
            )
            lines.append(line)
        return "\n".join([ l.rstrip("\\") for l in lines ])

    def evaluate(self, src):
        try:
            for result in self.runtime.eval(src):
                if result is not None:
                    print(format_yall(result))
        except Exception as e:
            print(e)
            #traceback.print_exc()
        if src:
            self.update_autocomplete()

    def update_autocomplete(self):
        env = self.runtime.env
        for name in reversed(list(env.privates.keys()) + env.names()):
            if name in self.complete.words:
                break
            self.complete.words.append(name)
