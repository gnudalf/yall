from .repl import run_repl
from ..lang.runtime import Runtime, print_exception
from ..wrappers import wrappers
from .test import run_tests
import sys
import os

VERSION = "yall 0.1"

def run(files, stdlib):
    rt = Runtime(native_stdlib=stdlib)
    try:
        for f in files:
            rt.use_file(f)
    except Exception as e:
        print_exception(e)

def run_wrapper(wrapper, files):
    if len(files) != 1:
        raise Exception("cannot map multiple files")
    rt = Runtime()
    rt.use_file(files[0], names=True)
    wrapper(rt)

def show_help():
    print("\n".join([
        VERSION,
        "    Yet Another Lispy Language",
        "",
        "Usage: yall [COMMAND [OPTIONS]] [FILES...]",
        "",
        "Commands:",
        "  test FILES...    run tests",
        "  repl FILES...    evaluate files and open repl",
        "",
        "Options:",
        "  --nostd      disables evaluation of yall stdlib",
        "  --help       show this message and exit",
        "  --vi         select vi mode in repl",
    ]))

def main(args):
    files = []
    options = []

    if len(args) == 1:
        return run_repl(vi_mode=True, files=files)
    for a in args[1:]:
        if os.path.isfile(a):
            files.append(a)
        else:
            options.append(a)

    stdlibs = "--nostd" not in options
    if not options:
        return run(files, stdlibs)

    if options[0] == "repl":
        return run_repl(vi_mode="--vi" in options, files=files, stdlibs=stdlibs)

    if options[0] == "test":
        return run_tests(files, stdlibs)

    if options[0] in wrappers:
        return run_wrapper(wrappers[options[0]], files)

    if options[0] in [ "version", "-v", "--version" ]:
        print(VERSION)
        return

    if options[0] in [ "help", "-h", "--help" ]:
        return show_help()

    print(f"invalid option {options[0]}")
    print("")
    show_help()
    sys.exit(1)

if __name__ == "__main__":
    main(sys.argv)
