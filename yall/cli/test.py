from ..lang.runtime import Runtime, print_exception
import sys
import click
import traceback

class TestResult:

    def __init__(self):
        self.run = 0
        self.failed = []

    def case(self, name, pos):
        click.echo(
            click.style(pos, fg="white") + " " +
            click.style(name, fg="yellow")
        )

    def record(self, run):
        self.run += 1

        if run.passed:
            success = click.style("PASS", fg="green")
        else:
            success = click.style("FAIL", fg="red")

        msg = "  " + success + " " + \
            click.style(run.pos.lc(), fg="white") + " " + \
            run.func + " " + ", ".join(run.args).replace("\n", "\\n")

        if not run.passed:
            self.failed.append((msg, run))

        click.echo(msg)
        if run.exception:
            print_exception(run.exception)


def run_tests(files, stdlibs):
    failed = []
    results = []
    for f in files:
        try:
            click.echo(click.style(f, fg="cyan"))
            tests = TestResult()
            rt = Runtime(test=tests, native_stdlib=stdlibs)
            rt.use_file(f, run_tests=True)
            color = "red" if tests.failed else "green"
            msg = f"{f}: {tests.run} tests run; {len(tests.failed)} failed"
            results.append(click.style(msg, fg=color))
            failed += tests.failed
            print("")
        except Exception as e:
            print_exception(e)
            results.append(click.style(f"{f}: {e}", fg="red"))
            #traceback.print_stack(e)
    print("")
    for msg, run in failed:
        print(f"{run.test_pos} {run.name}")
        click.echo(msg)
        if run.exception:
            print_exception(run.exception)
        print("")
    for r in results:
        click.echo(r)
    sys.exit(int(len(failed) > 0))
