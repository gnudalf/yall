from .http import wrap as http_wrapper

wrappers = {
    "http": http_wrapper
}
