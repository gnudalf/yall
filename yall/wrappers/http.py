from http.server import HTTPServer, BaseHTTPRequestHandler
from urllib.parse import urlparse, parse_qs
from yall.lang.runtime import print_exception

http_get = None
http_post = None

class Server(BaseHTTPRequestHandler):

    def do_GET(self):
        if http_get:
            args = parse_qs(urlparse(self.path).query)
            try:
                http_get(self.respond, self.path, args, dict(self.headers))
            except Exception as e:
                print_exception(e)
                self.respond(500, "Internal Server Error")
        else:
            self.respond(501, "Unsupported method GET")

    def do_POST(self):
        if not http_post:
            self.respond(501, "Unsupported method POST")
            return

        try:
            contlen = int(self.headers.get("Content-length"))
            body = self.rfile.read(contlen).decode("utf-8")
        except:
            body = ""
        http_post(self.respond, self.path, body, dict(self.headers))

    def respond(self, status, resp, headers=None):
        self.send_response(status)
        for key, value in (headers or {}).items():
            self.send_header(key, value)
        self.end_headers()
        self.wfile.write(bytes(resp, "utf-8"))


def wrap(rt):
    global http_get, http_post
    port = rt.get("http-port", 3000)
    address = rt.get("http-address", "")
    http_get = rt.get("http-get", False)
    http_post = rt.get("http-post", False)

    print(f"starting server on {address}:{port}")
    httpd = HTTPServer((address, port), Server)
    httpd.serve_forever()
