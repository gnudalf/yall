from .nodes import Env
from .parser import parse
from .lexer import lex
from .functions import functions
from .magic import magic
from .common import YallError
import math
import os
import pathlib
import click

def default_environment():
    env = Env()
    env.update(magic)
    env.update(functions)
    env.update({"math": vars(math)})
    return env

class Runtime:

    def __init__(self, env=None, test=None, native_stdlib=True):
        self.env = (env or default_environment()).make_child()
        self.env.runtime = self
        self.env.test = test
        self.test = test
        self.modules = {}
        if native_stdlib:
            self.use_file("yall/stdlib.yall", names=True)

    def child(self):
        return Runtime(
                self.env.make_child(),
                self.test,
                native_stdlib=False)

    def eval(self, src, env=None, name=""):
        for statement in parse(lex(src, name)):
            yield statement.eval(env or self.env)

    def use_file(self, src, run_tests=False, names=None, current_file=""):
        pretty_name = os.path.basename(src).replace(".yall", "")
        if pretty_name in self.modules:
            return self.import_module(
                    self.modules[pretty_name],
                    pretty_name,
                    names
            )

        filename = self._real_filename(src, current_file)
        with open(filename, "r") as f:
            self.env = self.env.make_child()
            self.env.privates.update({
                ".file": str(filename),
                ".name": pretty_name,
            })
            if run_tests:
                self.env.test = self.test
                self.env.privates[".test"] = True
            for i in self.eval(f.read(), name=pretty_name):
                pass
            child_env = self.env
            self.env = self.env.parent
            self.modules[pretty_name] = child_env
            return self.import_module(child_env, pretty_name, names)


    def import_module(self, child_env, pretty_name, names):
        if names:
            if names is True:
                for key, value in child_env.items():
                    self.env[key] = value
            else:
                for name in names:
                    try:
                        self.env[name] = child_env[name]
                    except KeyError:
                        raise YallError(f"cannot import {name} from {pretty_name}")
        else:
            self.env[pretty_name] = child_env
        return child_env

    def get(self, name, default=None):
        try:
            return self.env[name]
        except Exception as e:
            if default is None:
                raise e
            return default

    def set(self, name, value):
        self.env[name] = value

    def _real_filename(self, src, current_file):
        if not str(src).endswith(".yall"):
            src += ".yall"
        paths = []
        if current_file:
            paths.append(os.path.dirname(current_file))
        paths.append(".")

        if "YALLPATH" in os.environ:
            paths += [ p for p in os.environ["YALLPATH"].split(":") if p ]
        for p in map(pathlib.Path, paths):
            if os.path.isfile(p / src):
                return p / src
        return src


def print_exception(e):
    click.echo(click.style(e, fg="red"))
    if hasattr(e, "stack"):
        for frame in e.stack:
            print(f"    {frame[2]} {frame[1]}")
