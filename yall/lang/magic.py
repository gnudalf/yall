from .functions import Functions
import os, sys, random
from uuid import uuid4
import json
import subprocess
from datetime import datetime, timezone
import requests
from threading import Timer
from .magics.mqtt import mqtt

def _random(a=None):
    if type(a) == int:
        return int(random.random() * a)
    if type(a) == list:
        return random.choice(a)
    return random.random()

def _system(cmd):
    p = subprocess.Popen(
        cmd,
        shell=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE
    )
    return {
        "exitcode": p.wait(),
        "stdout": p.stdout.read().decode("utf-8"),
        "stderr": p.stderr.read().decode("utf-8"),
    }

def env(key = None, default = None):
    if key:
        if key in os.environ:
            return os.environ[key]
        elif default:
            return default
        else:
            raise Exception(f"{key} not set")
    return dict(os.environ)

def file_read(fn):
    with open(fn, "r") as f:
        return f.read()

def file_write(fn, content):
    with open(fn, "w") as f:
        f.write(content)

def date(*args):
    if type(args[0]) is str:
        return datetime.fromisoformat(args[0])
    return datetime(*args)

def timestamp(t):
    if type(t) in [float, int]:
        return datetime.utcfromtimestamp(t)
    if type(t) == datetime:
        return t.replace(tzinfo=timezone.utc).timestamp()

def http(options, success, error=None):
    if type(options) is str:
        options = {
            "url": options
        }
    try:
        resp = requests.get(options["url"])
        if resp.status_code >= 200 and resp.status_code < 300:
            success(resp.status_code, resp.text, resp.headers)
        else:
            if error:
                error(resp.status_code, resp.text, resp.headers)
    except Exception as e:
        if error:
            error(0, str(e), {})

def timer(delay, callback):
    timer = Timer(delay, callback)
    timer.start()
    return lambda: timer.cancel()


magic = Functions(**{
    "env!": env,
    "argv!": lambda: sys.argv[1:],
    "rand!": _random,
    "uuid!": uuid4,
    "json!-decode": json.loads,
    "json!-encode": json.dumps,
    "system!": _system,
    "input!": input,
    "file!-read": file_read,
    "file!-write": file_write,
    "now!": lambda: datetime.now(),
    "date!": date,
    "timestamp!": timestamp,
    "exit!": sys.exit,
    "http!": http,
    "timer!": timer,
    "mqtt!": mqtt
})
