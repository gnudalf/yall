from .lexer import T, Token
from .nodes import (
    ApplicationNode, RefNode, ConstantNode, DefinitionNode, LambdaNode,
    CondNode, special_forms, TypedNode
)
from .common import SyntaxError
import regex

cadr_variants = regex.compile("^c[ad]{2,}r$")

class Tokens:

    def __init__(self, tokens):
        self.tokens = tokens
        self.consumed = []

    def pop(self):
        self.consumed.append(self.tokens.pop(0))
        return self.consumed[-1]

    def __len__(self):
        return len(self.tokens)

    def peek(self):
        return self.tokens[0]

    def __getitem__(self, i):
        return self.tokens[i]

    def set_fallback(self):
        self.consumed = []

    def fallback(self):
        while self.consumed:
            self.tokens.insert(0, self.consumed.pop())


def parse(token_list):
    l = []
    tokens = Tokens(token_list)
    while tokens:
        l.append(parse_expression(tokens))
    return l

def parse_expression(tokens, t=None):
    if type(tokens) == list:
        tokens = Tokens(tokens)
    exp = _parse_expression(tokens, t)
    if tokens:
        if tokens.peek().match(T.DOT):
            return resolve_dots(exp, tokens, tokens.peek())
        if tokens.peek().match(T.BACKTICK):
            return infix_expression(exp, tokens, tokens.peek())
        if tokens.peek().match(T.BINARY_OP):
            return binary_infix_expression(exp, tokens, tokens.peek())
        if tokens.peek().match(T.MULTI_DOT):
            tokens.pop()
            exp.multiarg = True
            return exp
        if tokens.peek().match(T.DOUBLE_DOT):
            return function_composition(exp, tokens, t)
        if tokens.peek().match(T.THREAD):
            return lambda_sugar(exp, tokens, t)
        if tokens.peek().match(T.COLON):
            return typed_expression(exp, tokens, t)
    return exp

def _parse_expression(tokens, t=None):
    if not tokens:
        raise SyntaxError(f"EOF at {t.pos if t else 'unknown location'}")

    t = tokens.pop()

    if t.match(T.OPEN_PAREN):
        args = []
        thread = False
        while tokens and not tokens.peek().match(T.CLOSE_PAREN):
            if tokens.peek().match(T.THREAD):
                tokens.pop()
                thread = True
                continue
            exp = parse_expression(tokens, t)
            if hasattr(exp, "multiarg") and \
                    (not tokens or not tokens.peek().match(T.CLOSE_PAREN)):
                raise SyntaxError( \
                        f"expected CLOSE_PAREN after MULTI_DOT at {exp.pos()}")
            args.append(exp)
        if not tokens:
            last_position = (args[-1] if args else t).pos()
            raise SyntaxError(f"EOF at {last_position}")
        tokens.pop()
        if needs_implicit_lambda(args):
            return inline_lambda(args, t.pos)
        if thread:
            return threaded_application(args, t.pos)
        if not args:
            return ApplicationNode([])
        if isinstance(args[0], RefNode) and \
                cadr_variants.match(args[0].token.value):
            return cadr_expansion_sugar(args)
        if isinstance(args[0], RefNode) and \
                args[0].token.value == "cond":
            return cond_sugar(args)
        if len(args) == 1 and \
                type(args[0]) is ApplicationNode and \
                isinstance(args[0][0], RefNode) and \
                args[0][0].token.match(T.BINARY_OP):
                    return args[0]
        if hasattr(args[0], "composition"):
            return decompose_composition(args)
        args = transform_cadr_references(args)
        return application_node(args[0])(args)

    if t.match(T.SQUARE_BR_OPEN):
        return list_sugar(tokens, t)

    if t.match(T.CURLY_BR_OPEN):
        return obj_sugar(tokens, t)

    if t.match(T.NUMBER, T.BOOL, T.STRING, T.NIL):
        return ConstantNode(t)

    if t.match(T.ID):
        id = RefNode(t)
        if tokens:
            if tokens.peek().match(T.ASSIGN):
                tokens.pop()
                return DefinitionNode([
                    Token(T.ID, "def", id.pos),
                    id,
                    parse_expression(tokens)
                ])
        return id

    if t.match(T.NOT):
        return not_sugar(tokens, t)

    if t.match(T.PERCENT):
        return RefNode(t)

    if t.match(T.BINARY_OP):
        return RefNode(t)

    if t.match(T.DOT) and tokens and tokens.peek().match(T.ID):
        t = tokens.pop()
        t.value = "." + t.value
        return RefNode(t)

    raise SyntaxError(f"invalid token {t} at {t.pos}")

def obj_sugar(tokens, t):
    list_node = RefNode(Token(T.ID, "list", t.pos))
    obj_node  = RefNode(Token(T.ID, "obj", t.pos))
    args = []
    last_position = obj_node.pos()
    while tokens and not tokens.peek().match(T.CURLY_BR_CLOSE):
        key = _parse_expression(tokens)
        last_position = key.pos()
        if not tokens or not tokens.peek().match(T.COLON):
            raise SyntaxError(f"expected colon after {last_position.pos()}")
        tokens.pop()
        if tokens and tokens.peek().match(T.COLON):
            tokens.pop()
        elif type(key) == RefNode:
            key = ConstantNode(Token(T.STRING, key.token.value, last_position))
        value = parse_expression(tokens)
        args.append(ApplicationNode([ list_node, key, value]))
    if not tokens:
        raise SyntaxError(f"EOF at {last_position}")
    tokens.pop()
    return ApplicationNode([ obj_node ] + args)

def list_sugar(tokens, t):
    args = []
    last_position = t.pos
    while tokens and not tokens.peek().match(T.SQUARE_BR_CLOSE):
        first = parse_expression(tokens)
        last_position = first.pos()
        args.append(first)
    if not tokens:
        raise SyntaxError(f"EOF at {last_position}")
    tokens.pop()
    if len(args) == 1 and hasattr(args[0], "composition"):
        start = args[0][2][0]
        end = args[0][2][1][0]
        return ApplicationNode([
            RefNode(Token(T.ID, "seq", t.pos)),
            start,
            end
        ])
    return ApplicationNode([ RefNode(Token(T.ID, "list", t.pos)) ] + args)

def nested_cadrs(exp, ads):
    while ads:
        exp = ApplicationNode([
            RefNode(Token(T.ID, f"c{ads.pop()}r", exp.pos())),
            exp
        ])
    return exp

def inline_lambda(args, start_pos):
    var_count = 1
    max_number = 0
    for a in args:
        if isinstance(a, RefNode) and a.token.match(T.PERCENT):
            if len(a.token.value) > 1:
                number = int(a.token.value[1:])
                if number > max_number:
                    max_number = number
            else:
                a.token.value += str(var_count)
                var_count += 1
    return LambdaNode([
        RefNode(Token(T.ID, "lambda", start_pos)),
        [
            RefNode(Token(T.ID, f"%{i}", start_pos))
            for i in range(1, max([var_count, max_number+1]))
        ],
        application_node(args[0])(args)
    ])

def application_node(n):
    if isinstance(n, RefNode) and n.token.value in special_forms:
        return special_forms[n.token.value]
    return ApplicationNode

def not_sugar(tokens, t):
    if not tokens:
        raise SyntaxError(f"EOF; expected not expression after {t.pos}")
    first_node = RefNode(Token(T.ID, "not", t.pos))
    return ApplicationNode([ first_node, parse_expression(tokens, t)])

def threaded_application(args, start_pos):
    exp = args[0]
    for arg in args[1:]:
        if type(arg) == LambdaNode:
            arg = ApplicationNode([arg])
        arg.nodes.append(exp)
        exp = arg
    return exp

def cadr_expansion_sugar(args):
    pos = args[0].pos()
    ads = reversed(args[0].token.value[1:-1])
    exp = ApplicationNode([ RefNode(Token(T.ID, f"c{next(ads)}r", pos)) ] + args[1:])
    for x in ads:
        exp = ApplicationNode([ RefNode(Token(T.ID, f"c{x}r", pos)) ] + [ exp ])
    return exp

def resolve_dots(exp, tokens, t):
    while tokens and tokens.peek().match(T.DOT):
        last = tokens.pop()
        second = _parse_expression(tokens, last)
        if isinstance(second, RefNode):
            second = ConstantNode(Token(
                T.STRING,
                second.token.value,
                second.pos())
            )
        exp = ApplicationNode([
            RefNode(Token(T.ID, "get", last.pos)),
            exp,
            second
        ])
    return exp

def binary_infix_expression(exp, tokens, t):
    args = [exp]
    while tokens and tokens.peek().match(T.BINARY_OP):
        args.append(_parse_expression(tokens, t))
        if not tokens:
            raise SyntaxError(f"EOF after {op.pos()}, expected backtick")
        args.append(_parse_expression(tokens, t))
    return nest_binary_operations(args)

def nest_binary_operations(args):
    out = []
    ops = []
    for a in args:
        if is_binary_operator(a):
            while ops and precedence(ops[-1]) >= precedence(a):
                out.append(ops.pop())
            ops.append(a)
        else:
            out.append(a)
    while ops:
        out.append(ops.pop())
    def consume_rpn():
        if not is_binary_operator(out[-1]):
            return out.pop()
        exp, right, left = (out.pop(), consume_rpn(), consume_rpn())
        return ApplicationNode([ exp, left, right ])
    return consume_rpn()

def is_binary_operator(x):
    return isinstance(x, RefNode) and x.token.match(T.BINARY_OP)

precedence_rules = {
    "<": 0,
    "<=": 0,
    ">": 0,
    ">=": 0,
    "==": 0,
    "!=": 0,
    "+": 1,
    "-": 1,
    "/": 2,
    "*": 2,
}

def precedence(x):
    return precedence_rules[x.token.value]

def infix_expression(exp, tokens, t):
    last = tokens.pop()
    name = _parse_expression(tokens, last)
    if not tokens:
        raise SyntaxError(f"EOF after {name.pos()}, expected backtick")
    last = tokens.pop()
    args = [ name, exp, _parse_expression(tokens, last) ]
    if needs_implicit_lambda(args):
        return inline_lambda(args, t.pos)
    return application_node(name)(args)

def function_composition(first, tokens, t):
    calls = [first]
    while tokens and tokens.peek().match(T.DOUBLE_DOT):
        t = tokens.pop()
        if not tokens:
            raise SyntaxError(f"EOF after DOUBLE_DOT at {t.pos}")
        calls.append(_parse_expression(tokens, t))
    exp = RefNode(Token(T.PERCENT, "%", t.pos))
    while calls:
        func = calls.pop()
        exp = application_node(func)([ func, exp ])
    func = LambdaNode([
        RefNode(Token(T.ID, "lambda", t.pos)),
        ApplicationNode([
            RefNode(Token(T.PERCENT, "%", t.pos))
        ]),
        exp
    ])
    func.composition = True
    return func

def decompose_composition(args):
    expr = args[0][2]
    def replace_percent(expr, arg):
        for i,x in zip(range(len(expr)), expr):
            if isinstance(x, RefNode) and x.token.match(T.PERCENT):
                expr.nodes[i] = arg
                return
            if isinstance(x, ApplicationNode):
                replace_percent(x, arg)
    replace_percent(expr, args[1])
    return expr

def needs_implicit_lambda(args):
    return [
        a for a in args
        if isinstance(a, RefNode) and a.token.match(T.PERCENT)
    ]

def cond_sugar(args):
    pos = args[0].pos()
    expr = ApplicationNode([
        RefNode(Token(T.ID, "error", pos)),
        ConstantNode(Token(T.STRING, "out of cases", pos)),
    ])
    cases = args[1:]
    while cases:
        case = cases.pop()
        expr = CondNode([
            RefNode(Token(T.ID, "if", case.pos())),
            case[0],
            case[1],
            expr
        ])
    return expr

def lambda_sugar(exp, tokens, t):
    thread = tokens.pop()
    if not isinstance(exp, ApplicationNode):
        exp = ApplicationNode([exp])
    params = []
    lists = []
    for a in exp:
        if isinstance(a, ApplicationNode) and len(a) \
                and isinstance(a[0], RefNode) and str(a[0]) == "list":
            params.append(RefNode(Token(T.PERCENT, f"%{len(lists)+1}", a.pos())))
            lists.append(a)
        else:
            params.append(a)
    for i, li in zip(range(len(lists)), lists):
        ads = []
        for la in li[1:]:
            params.append(DefinitionNode([
                Token(T.ID, "def", la.pos()),
                la,
                nested_cadrs(
                    RefNode(Token(T.PERCENT, f"%{i+1}", la.pos())),
                    ["a"] + ads
                )
            ]))
            ads.append("d")
    return LambdaNode([
        RefNode(Token(T.ID, "lambda", thread.pos)),
        ApplicationNode(params),
        parse_expression(tokens, t)
    ])

def cadr_reference_to_lambda(a):
    ads = list(a.token.value[1:-1])
    percent = RefNode(Token(T.PERCENT, "%", a.pos()))
    return LambdaNode([
        RefNode(Token(T.ID, "lambda", a.pos())),
        ApplicationNode([ percent ]),
        nested_cadrs(percent, ads)
    ])

def transform_cadr_references(args):
    return [
        cadr_reference_to_lambda(a)
        if isinstance(a, RefNode) and cadr_variants.match(a.token.value) else a
        for a in args
    ]

def typed_expression(exp, tokens, t):
    colon = tokens.pop()
    return TypedNode([
        RefNode(Token(T.ID, "typed", exp.pos())),
        exp,
        _parse_expression(tokens)
    ])
