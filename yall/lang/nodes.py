from .lexer import T, Token
from .common import UndefinedError, TestRun
from .functions import (
    format_yall, NativeFunction, Tailcall, obj_to_list, LazyFunction
)
import traceback

class TestResult:

    def __init__(self):
        self.run = 0
        self.failed = []

    def case(self, pos, name):
        pass

    def remember(self, name, passed):
        self.run += 1
        if not passed:
            self.failed.append(name)

    def record(self, run):
        self.remember(run.name, run.passed)

class Env(dict):

    def __init__(self, parent=None, runtime=None, test=None, **kwargs):
        self.update(kwargs)
        self.parent = parent
        self.test = test
        self.runtime = runtime
        self.continuation = []
        self.privates = {
            ".env": self,
            ".parent": self.parent,
            ".file": "",
            ".name": "",
            ".test": False,
        }

    def make_child(self, test=None):
        child = Env(self, self.runtime, self.test if test else None)
        for keep in [".file", ".name", ".test"]:
            child.privates[keep] = self.privates[keep]
        child.continuation = self.continuation
        return child

    def find(self, name):
        if name in self or name in self.privates:
            return self
        return self.parent.find(name)

    def get(self, name):
        if name in self:
            return self[name]
        return self.privates[name]

    def depth(self):
        if self.parent:
            return 1 + self.parent.depth()
        else:
            return 1

    def names(self):
        parents = []
        if self.parent:
            parents = self.parent.names()
        return parents + list(self.keys())

class Node:

    def eval(self, env):
        raise Exception("unimplemented")

    def test(self, env):
        return self.eval(env)

    def pos(self):
        raise Exception("unimplemented")

    def find_recursive_calls(self, name):
        return []

    def find_recursive_tailcall(self, name):
        return []

    def all_tail_expressions(self):
        return [self]

    def is_recursive_call(self, name):
        return False

    def cps_transformable(self, name):
        return False

    def transform_cps(self, name):
        return self

class ApplicationNode(Node):
    def __init__(self, nodes):
        self.nodes = nodes
        self.tailcall = None

    def pos(self):
        return ( self.nodes[0] or self.nodes[1] ).pos()

    def __getitem__(self, item):
        return self.nodes[item]

    def __len__(self):
        return len(self.nodes)

    def is_recursive_call(self, name):
        return len(self) and isinstance(self.nodes[0], RefNode) and \
                str(self.nodes[0]) == name

    def find_recursive_calls(self, name):
        if self.is_recursive_call(name):
            return [self]
        rec = []
        for n in self.nodes[1:]:
            if not isinstance(n, Node):
                continue
            for recursive in n.find_recursive_calls(name):
                rec.append(recursive)
        return rec

    def find_recursive_tailcall(self, name):
        if str(self.nodes[0]) == name:
            return [self]
        return []

    def _exception(self, e, func = None):
        if not hasattr(e, "stack"):
            e.stack = []
        if e.stack and e.stack[-1][0] is func:
            raise e
        e.stack.append((func, format_yall(func), self.pos()))
        raise e

    def _eval(self, env):
        func = self.nodes[0].eval(env)
        if isinstance(func, LazyFunction):
            args = self.nodes[1:]
        else:
            args = [
                a if isinstance(a, DefinitionNode) else a.eval(env)
                for a in self.nodes[1:]
            ]
        if self.tailcall:
            self.tailcall.args = args
            return (self.tailcall, None, None)
        if hasattr(func, "tailcall") and func.tailcall:
            for application in func.tailcall:
                application.tailcall = Tailcall(func)
        try:
            if isinstance(func, (NativeFunction, LazyFunction)):
                result = func(env, *args)
            else:
                result = func(*args)
            while type(result) is Tailcall:
                args = result.args
                result = result.func(*args)
                if type(result) is Tailcall and \
                        not func.name.endswith("!") and \
                        result.args == args:
                    raise Exception(
                            "infinite recursive tailcall of " + \
                            f"{func.name} with args {args}")
            return (result, func, args)
        except Exception as e:
            self._exception(e, func)

    def eval(self, env):
        if not len(self):
            return None
        return self._eval(env)[0]

    def test(self, env):
        return self._eval(env)

    def __str__(self):
        return "(" + " ".join(map(str, self.nodes)) + ")"

class ResetNode(ApplicationNode):

    def eval(self, env):
        sub_env = env.make_child()
        sub_env.continuation = [lambda x:x]
        return self.nodes[1].eval(sub_env)


class ShiftNode(ApplicationNode):

    def eval(self, env):
        env.update({
            str(self.nodes[1]): lambda x:x
        })
        return self.nodes[2].eval(env)

class QuoteNode(ApplicationNode):

    def eval(self, env):
        return self.nodes[1]

class EvalNode(ApplicationNode):

    def eval(self, env):
        return self.nodes[1].eval(env).eval(env)

class DescribeNode(ApplicationNode):

    def eval(self, env):
        expr = self.nodes[1]
        definition = ""
        if type(expr) == RefNode:
            definition = expr.token.value
            expr = expr.eval(env)
        string = expr.description if hasattr(expr, "description") else str(expr)
        if definition:
            return f"(def {definition} {string})"
        else:
            return string

class TokenNode(Node):
    def __init__(self, token):
        self.token = token

    def pos(self):
        return self.token.pos

    def value(self):
        return self.token.value


class ConstantNode(TokenNode):

    def __init__(self, token):
        super().__init__(token)
        if token.match(T.NUMBER):
            self.value = self.parse_number()
        elif token.match(T.BOOL):
            self.value = self.parse_bool()
        elif token.match(T.NIL):
            self.value = None
        elif token.match(T.STRING):
            self.value = token.value

    def parse_number(self):
        v = self.token.value
        modificator = 1
        while v.startswith("-"):
            modificator *= -1
            v = v[1:]
        base = 10
        if v.startswith("0b"):
            base = 2
        if v.startswith("0o"):
            base = 8
        if v.startswith("0x"):
            base = 16
        try:
            return int(v, base) * modificator
        except ValueError:
            return float(v) * modificator

    def parse_bool(self):
        return self.token.value == "true"

    def eval(self, env):
        return self.value

    def __str__(self):
        return format_yall(self.value)

class DynamicConstantNode(Node):
    def __init__(self, value):
        self.value = value

    def eval(self, env):
        return self.value

class RefNode(TokenNode):

    def __init__(self, token):
        super().__init__(token)

    def eval(self, env):
        name = self.token.value
        try:
            return env.find(name).get(name)
        except (KeyError, AttributeError) as e:
            raise UndefinedError(f"'{name}' is undefined at {self.pos()}")

    def __str__(self):
        ma = "..." if hasattr(self, "multiarg") else ""
        return self.token.value + ma

class DefinitionNode(ApplicationNode):
    def __init__(self, tokens):
        super().__init__(tokens)
        if len(self) == 1:
            raise Exception(f"definition without name")
        self.name = self[1].token.value
        if len(self) == 2:
            raise Exception(f"missing value for definition '{self.name}' at {self[1].token.pos}")
        if not self[1].token.match(T.ID, T.PERCENT):
            raise Exception(f"definition requires a name but got '{self.name}' at {self[1].token.pos}")
        if len(self) > 3:
            raise Exception(f"extra parameters for definition '{self.name}' at {self[1].token.pos}")
        self.value = self[2]

    def __str__(self):
        return f"(def {self.name} {self.value})"


    def eval(self, env):
        if self.name in env:
            self._exception(Exception(f"cannot redefine '{self.name}'"), "def")
        try:
            if self.value.cps_transformable(self.name):
                self.value.transform_cps(self.name)
            value = self.value.eval(env)
            if isinstance(value, Func):
                value.name = self.name
                value.recursion = value.find_recursive_calls(self.name)
                value.tailcall = value.find_recursive_tailcall(self.name)
            env[self.name] = value
            return value
        except Exception as e:
            self._exception(e, "def")

class Func:

    def __init__(
            self,
            params,
            types,
            expression,
            env,
            multiarg = False,
            description = ""
        ):
        self.params = params
        self.types = types
        self.expression = expression
        self.env = env
        self.name = ""
        self.description = description
        self.multiarg = multiarg
        self.recursion = []
        self.tailcall = []
        self.nested_continuation = []

    def transform_cps(self, name):
        pass

    def find_recursive_tailcall(self, name):
        if not self.name or not isinstance(self.expression, ApplicationNode):
            return
        return self.expression.find_recursive_tailcall(name)

    def find_recursive_calls(self, name):
        if not self.name or not isinstance(self.expression, ApplicationNode):
            return []
        return self.expression.find_recursive_calls(name)

    def __call__(self, *args):
        env = self.env.make_child(test=True)
        argv = {}
        normal_args = [a for a in args if not isinstance(a, DefinitionNode)]
        named_args = {
            a.name: a.eval(env)
            for a in args if isinstance(a, DefinitionNode)
        }
        multiarg_name = ""
        if self.multiarg:
            multiarg_name = list(self.params.keys())[-1]
        i = 0
        missing = []
        for name, default in self.params.items():
            if name == multiarg_name and not missing:
                argv[name] = normal_args[i:]
                env.update(argv)
                break
            if len(normal_args) > i and normal_args[i] is not None:
                argv[name] = normal_args[i]
            elif name in named_args:
                argv[name] = named_args[name]
            elif not missing and isinstance(default, Node):
                argv[name] = default.eval(env)
            elif len(normal_args) > i and normal_args[i] is None:
                argv[name] = None
            else: # missing arg
                missing.append(name)
            i += 1
            env.update(argv)
        if missing:
            if not args:
                return self
            return Func(
                { k:v for k,v in self.params.items() if k in missing },
                self.types,
                self.expression,
                env,
                self.multiarg,
                "(" + self.description + \
                        " ".join([
                            format_yall(argv[k])
                            for k in self.params if k not in missing
                        ]) + ")",
            )

        if self.nested_continuation:
            val = args[0]
            val_name = list(self.params.keys())[0]
            cont_name = str(self.expression[0])
            while self.nested_continuation:
                e = self.nested_continuation.pop()
                e.update({
                    val_name: val,
                    cont_name: lambda x:x
                })
                val = self.expression.eval(e)
            env.update({
                val_name: val,
                cont_name: lambda x:x
            })
            return self.expression.eval(env)

        return self.expression.eval(env)

    def __str__(self):
        return " ".join([ p for p in [ "𝝺", self.name ] if p ])

class LambdaNode(ApplicationNode):
    def __init__(self, tokens):
        super().__init__(tokens)
        self.params = {}
        self.types = {}
        if len(self) == 1:
            raise Exception(f"lambda is missing an expression")
        self._multiarg = isinstance(self[1], ApplicationNode) \
                and len(self[1]) \
                and hasattr(self[1][-1], "multiarg")
        if len(self) == 2:
            self.expression = self[1]
            return
        if len(self) > 3:
            raise Exception(f"lambda can not have extra arguments")
        self.update_params()
        self.expression = self[2]

    def update_params(self):
        for n in self[1]:
            type = None
            default = None
            param = ""
            if isinstance(n, TypedNode):
                type = n.type()
                n = n.exp()
            if isinstance(n, RefNode):
                param = n.token.value
            elif isinstance(n, DefinitionNode):
                param = n.name
                default = n.value
            else:
                raise Exception(f"unsupported lambda param {n} at {n.pos()}")
            self.params[param] = default
            self.types[param] = type

    def eval(self, env):
        try:
            return self.build_nested_continuation(env) \
                    or Func(
                        self.params,
                        self.types,
                        self.expression,
                        env,
                        self._multiarg,
                        str(self)
                    )
        except Exception as e:
            self._exception(e, "lambda")

    def build_nested_continuation(self, env):
        if len(self.params) != 1 or \
                type(self.expression) is not ApplicationNode or \
                not isinstance(self.expression[0], RefNode):
            return
        try:
            inner = self.expression[0].eval(env)
            if hasattr(inner, "expression") and \
                    inner.expression is self.expression:
                inner.nested_continuation.append(env)
                return inner
        except Exception as e:
            pass

    def cps_transformable(self, name):
        if len(self) < 3:
            return False
        recursion = self.find_recursive_calls(name)
        tailcall = self[2].find_recursive_tailcall(name)
        return len(recursion) == 1 and not tailcall

    def transform_cps(self, name):
        self[1].nodes.append(DefinitionNode([
                RefNode(Token(T.ID, "def")),
                RefNode(Token(T.PERCENT, "%k")),
                LambdaNode([
                    RefNode(Token(T.ID, "lambda")),
                    ApplicationNode([
                        RefNode(Token(T.PERCENT, "%")),
                    ]),
                    RefNode(Token(T.PERCENT, "%")),
                ])
            ]))
        self.update_params()
        self[2].transform_cps(name)

    def __str__(self):
        params = ' '.join([
            f"(def {k} {v})" if v else k
            for k, v in self.params.items()
        ])
        if self._multiarg:
            params += "..."
        return f"(lambda ({params}) {self.expression})"

class CondNode(ApplicationNode):

    def __init__(self, tokens):
        super().__init__(tokens)
        self.type = self[0].token.value

    def all_tail_expressions(self):
        tailcalls = []
        for tc in self[2:]:
            for t in tc.all_tail_expressions():
                tailcalls.append(t)
        return tailcalls

    def transform_cps(self, name):
        for i,tc in zip(range(2, len(self)), self[2:]):
            if tc.find_recursive_calls(name):
                recursive = None
                for x in tc:
                    if x.is_recursive_call(name):
                        recursive = x
                        break # TODO multiple recursive calls (eg fib) in args
                if not recursive:
                    # TODO multiple nested applications until recursion
                    raise Exception("unsupported cps")
                call = LambdaNode([
                    RefNode(Token(T.ID, "lambda")),
                    ApplicationNode([
                        RefNode(Token(T.PERCENT, "%v")),
                    ]),
                    ApplicationNode([
                        RefNode(Token(T.PERCENT, "%k")),
                        ApplicationNode([
                            RefNode(Token(T.PERCENT, "%v"))
                            if x.is_recursive_call(name)
                            else x
                            for x in tc
                        ])
                    ])
                ])
                recursive.nodes.append(call)
                self.nodes[i] = recursive
            else:
                self.nodes[i] = ApplicationNode([
                    RefNode(Token(T.PERCENT, "%k")),
                    tc
                ])

    def find_recursive_tailcall(self, name):
        if len(self) < 2:
            return []
        tailcalls = []
        for node in self[1:]:
            for tc in node.find_recursive_tailcall(name):
                tailcalls.append(tc)
        return tailcalls

    def eval(self, env):
        try:
            predicate = self[1].eval(env)
            _else = self[3] if len(self) == 4 else NIL
            return (self[2] if predicate else _else).eval(env)
        except Exception as e:
            self._exception(e, self.type)

class TestNode(ApplicationNode):

    def __init__(self, tokens):
        super().__init__(tokens)
        self.name = ""
        self.first_test = 1
        if isinstance(self[1], ConstantNode) and self[1].token.match(T.STRING):
            self.name = self[1].token.value
            self.first_test = 2

    def eval(self, env):
        if not env.test:
            return
        try:
            env.test.case(self.name, self.pos())
            for stmt in self[self.first_test:]:
                result = None
                func = format_yall(stmt)
                args = []
                exception = None
                try:
                    if isinstance(stmt, ApplicationNode):
                        result = stmt.test(env)
                        passed = result[0]
                        func = format_yall(result[1])
                        args = [ format_yall(a) for a in result[2] ]
                    else:
                        print("       ", str(stmt))
                        passed = stmt.test(env)
                except Exception as e:
                    #traceback.print_exc()
                    exception = e
                    passed = False

                env.test.record(TestRun(
                    name=self.name,
                    passed=passed,
                    pos=stmt.pos(),
                    test_pos=self.pos(),
                    func=func,
                    args=args,
                    exception=exception,
                ))
        except Exception as e:
            self._exception(e, "test")

class UseNode(ApplicationNode):

    def eval(self, env):
        try:
            if isinstance(self[1], RefNode):
                name = self[1].token.value
            else:
                name = self[1].eval(env)
            if not env.runtime:
                raise Exception(f"use '{name}' failed due to missing runtime")
            return env.runtime.use_file(
                    name,
                    names=self._import_names(env),
                    current_file=env.privates[".file"]
                )
        except Exception as e:
            self._exception(e, "use")

    def _import_names(self, env):
        names = []
        for n in self[2:]:
            try:
                if isinstance(n, RefNode):
                    names.append(n.token.value)
                else:
                    names.append(n.eval(env))
            except UndefinedError as e:
                if isinstance(n, RefNode):
                    names.append(n.value())
                else:
                    raise e
        return names

class CatchNode(ApplicationNode):

    def eval(self, env):
        try:
            return self[1].eval(env)
        except Exception as e:
            if len(self) == 3:
                if isinstance(self[2], LambdaNode):
                    return ApplicationNode([
                        self[2],
                        DynamicConstantNode(str(e))
                    ]).eval(env)
                result = self[2].eval(env)
                if callable(result):
                    return result(str(e))
                return result

class TypedNode(ApplicationNode):

    def exp(self):
        return self[1]

    def type(self):
        return self[2]

    def eval(self, env):
        raise Exception("unimplemented")

NIL = ConstantNode(Token(T.NIL, "nil"))

special_forms =  {
    "lambda": LambdaNode,
    "def": DefinitionNode,
    "if": CondNode,
    "test": TestNode,
    "use": UseNode,
    "catch": CatchNode,
    "reset": ResetNode,
    "shift": ShiftNode,
    "quote": QuoteNode,
    "eval": EvalNode,
    "describe": DescribeNode,
}
