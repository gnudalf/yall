from .common import YallError
from functools import reduce
from datetime import datetime
import inspect

def format_yall(x, strings=True):
    if isinstance(x, str):
        return '"'+x+'"' if strings else x
    if isinstance(x, list):
        return f"[{' '.join([ format_yall(i) for i in x ])}]"
    if isinstance(x, dict):
        return "{" + \
            ' '.join([
                format_yall(k)+':'+format_yall(v)
                for k, v in x.items()
            ]) + "}"
    if x is True or x is False:
        return str(x).lower()
    if isinstance(x, datetime):
        return x.isoformat()
    if x is None:
        return "nil"
    stdfunc_key = [ k for k,v in functions.items() if x is v ]
    if stdfunc_key:
        return "𝝺 " + stdfunc_key[0]
    return str(x)

class Functions(dict):
    def __init__(self, **kwargs):
        self.__dict__ = kwargs
        self.update(kwargs)

def seq(a, b=None):
    start = a if b else 1
    end = b if b else a
    return list(range(start, end+1))

def make_list(*vals):
    if len(vals) == 1:
        if type(vals[0]) == dict:
            return [ [ k, v ] for k, v in vals[0].items() ]
    return list(vals)

def get(l, i):
    if type(i) == str and hasattr(l, i):
        value = getattr(l, i)
        return value() if callable(value) else value
    try:
        return l[i]
    except IndexError:
        raise Exception(f"'{i}' not in {'obj' if type(l) == dict else 'list'}")

def substr(s, a, b=None):
    return s[a:b+1] if b else s[a:]

def append(l, *values):
    for v in values:
        l.append(v)
    return l

def mutating_cons(x, l):
    if isinstance(l, list):
        l.insert(0, x)
        return l
    if isinstance(l, dict):
        l.update(x)
        return l
    raise Exception(f"cons! only works on lists and objects")

def error(message="unnamed error"):
    raise YallError(message)

def obj_to_list(x):
    if isinstance(x, dict):
        return [ [ k, v ] for k, v in x.items() ]
    return x

def compare(l):
    def inner(*vals):
        a = vals[0]
        for v in vals[1:]:
            if not l(a, v):
                return False
            a = v
        return True
    return inner

def _not(*vals):
    for v in vals:
        if not v:
            return True
    return False

class NativeFunction:

    def __init__(self, func=None, name="", comparator=None):
        self.func = func
        self.name = name
        self.comparator = comparator
        if not func and comparator:
            self.func = self.compare

    def __call__(self, env, *args):
        try:
            if self.func:
                return self.func(*args)
            else:
                return getattr(self, f"call_{self.name}")(env, *args)
        except TypeError:
            if not args:
                return self
            spec = inspect.getfullargspec(self.func)
            bound_args = dict(zip(spec.args, args))
            return lambda *a: self.func(*(args + a))

    def compare(self, *args):
        suspended = lambda arg: hasattr(arg, "to_list")
        if len(args) == 2 and suspended(args[0]) and suspended(args[1]):
            return args[0].compare_to(args[1], self.comparator)
        return self.comparator(*[
            arg.to_list() if suspended(arg) else arg
            for arg in args
        ])

    def tailcall_safe(self, f):
        def inner(*args):
            result = f(*args)
            while type(result) is Tailcall:
                args = result.args
                result = f(*args)
            return result
        return inner

    def call_map(self, env, fn, l):
        if callable(fn):
            if hasattr(fn, "tailcall"):
                fn = self.tailcall_safe(fn)
            return [ fn(i) for i in obj_to_list(l) ]
        else:
            return [ fn for i in obj_to_list(l) ]

    def call_filter(self, env, fn, l):
        func = self.tailcall_safe(fn) if hasattr(fn, "tailcall") else fn
        return [ i for i in obj_to_list(l) if func(i) ]

class Tailcall:

    def __init__(self, func):
        self.func = func
        self.args = []

    def __str__(self):
        return f"(tailcall ({self.func} {' '.join(map(str, self.args))}))"


class Suspension:

    def __init__(self, env, head, tail):
        self.env = env
        self.head = head
        self.tail = tail
        self._head = None
        self._tail = None

    def car(self):
        if not self._head:
            self._head = self.head.eval(self.env)
        return self._head

    def cdr(self):
        if not self._tail:
            self._tail = self.tail.eval(self.env) or []
        return self._tail

    def __iter__(self):
        yield self.car()
        rest = self.tail.eval(self.env)
        while isinstance(rest, Suspension):
            yield rest.car()
            rest = rest.tail.eval(rest.env)
        if isinstance(rest, list):
            for v in rest:
                yield v

    def compare_to(self, other, func):
        for x, y in zip(self, other):
            if not func(x, y):
                return False
        return True

    def to_list(self):
        return list(self)

    def __str__(self):
        return format_yall(self.to_list())

class Type:

    def __init__(self, type):
        self.type = type

class LazyFunction:
    pass

class Car(LazyFunction):
    def __call__(self, env, *args):
        the_list = args[0].eval(env)
        if isinstance(the_list, Suspension):
            return the_list.car()
        return obj_to_list(the_list)[0]

class Cdr(LazyFunction):
    def __call__(self, env, *args):
        the_list = args[0].eval(env)
        if isinstance(the_list, Suspension):
            return the_list.cdr()
        return obj_to_list(the_list)[1:]

class Cons(LazyFunction):
    def __call__(self, env, *args):
        x = args[0].eval(env)
        l = args[1].eval(env) if len(args) > 1 else None
        if l is None:
            l = []
        if isinstance(l, list):
            return [ x ] + l
        if isinstance(l, dict):
            new = dict(l)
            new.update(x)
            return new
        if isinstance(l, str) and isinstance(x, str):
            return x+l
        raise Exception(f"cons only works on lists, objects and strings")

class Conss(LazyFunction):
    def __call__(self, env, *args):
        tail = Nil() if len(args) < 2 else args[1]
        return Suspension(env, args[0], tail)

class And(LazyFunction):
    def __call__(self, env, *args):
        for i in args:
            v = i.eval(env)
            if not v:
                break
        return v

class Or(LazyFunction):
    def __call__(self, env, *args):
        for i in args:
            v = i.eval(env)
            if v:
                break
        return v

class Set(LazyFunction):
    def __call__(self, env, *args):
        name = str(args[0])
        value = args[1].eval(env)
        env.find(name).update({
            name: value
        })
        return value


class Nil:
    def eval(self, env):
        return None

def tolist(v):
    if isinstance(v, Suspension):
        return v.to_list()
    if isinstance(v, str):
        return list(v)
    return v


only_core_functions = {

    "car": Car(),
    "cdr": Cdr(),
    "cons": Cons(),
    "conss": Conss(),
    "and": And(),
    "or": Or(),
    "tolist": tolist,
    "set!": Set(),

    # maths
    "+": NativeFunction(lambda a, b: a+b),
    "-": NativeFunction(lambda a, b: a-b),
    "*": NativeFunction(lambda a, b: a*b),
    "/": NativeFunction(lambda a, b: a/b),
    "mod": lambda *v: reduce(lambda a,b: a % b, v),

    # relational
    "==": NativeFunction(comparator=lambda a, b: a == b),
    "!=": NativeFunction(comparator=lambda a, b: a != b),
    "<":  NativeFunction(comparator=lambda a, b: a < b),
    ">":  NativeFunction(comparator=lambda a, b: a > b),
    "<=": NativeFunction(comparator=lambda a, b: a <= b),
    ">=": NativeFunction(comparator=lambda a, b: a >= b),

    # lists
    "obj": lambda *v: { l[0]: l[1] for l in v },
    "cons!": mutating_cons,
    "append!": append,
    "pop!": lambda l, i=0: l.pop(i),

    # types
    "int": int,
    "float": float,
    "str": lambda *v: "".join([ format_yall(s, strings=False) for s in v ]),
    "number?": lambda x: isinstance(x, (int, float)) and not isinstance(x, bool),
    "float?": lambda x: isinstance(x, float),
    "str?": lambda x: isinstance(x, str),
    "list?": lambda x: isinstance(x, (list, Suspension)),
    "obj?": lambda x: isinstance(x, dict),
    "bool": bool,
    "bool?": lambda x: x is True or x is False,
    "nil?": lambda x: x is None,
    "func?": callable,
    "is?": lambda a, b: a is b,

    "error": error,
    "print": lambda *v: print(*[format_yall(i, strings=False) for i in v]),
    "apply": NativeFunction(lambda f, v: f(*v.to_list()) if isinstance(v, Suspension) else f(*v)),
    "do": lambda *v: v[-1],
}

# extra functions should be implemented in yall
extra_functions = {
    "not": _not, # ?

    "eq": compare(lambda a,b: a == b),
    "ne": compare(lambda a,b: a != b),
    "gt": compare(lambda a,b: a > b),
    "ge": compare(lambda a,b: a >= b),
    "lt": compare(lambda a,b: a < b),
    "le": compare(lambda a,b: a <= b),

    "add": lambda *v: reduce(lambda a,b: a + b, v),
    "sub": lambda *v: reduce(lambda a,b: a - b, v),
    "mul": lambda *v: reduce(lambda a,b: a * b, v, 1),
    "div": lambda *v: reduce(lambda a,b: a / b, v),

    "zero?": lambda x:
        isinstance(x, (int, float)) and not x and not isinstance(x, bool),

    "list": make_list,

    "len": lambda l: len(l),
    "seq": seq,
    "map": NativeFunction(name="map"),
    "filter": NativeFunction(name="filter"),
    "has?": lambda l, v: v in l,
    "get": get,
    "sort": lambda *v: sorted(list(*v)),
    "reverse": lambda *v: [x for x in reversed(list(*v))],
    "min": min,
    "max": max,

    "substr": substr,
    "join": NativeFunction(lambda delimiter, values: delimiter.join(values)),
    "split": lambda s, seperator=" ": s.split(seperator),
    "trim": lambda s: s.strip(),
    "char": chr,
    "ascii": ord,
}

core_functions = Functions(**only_core_functions)

all_functions = {}
all_functions.update(only_core_functions)
all_functions.update(extra_functions)
functions = Functions(**all_functions)
