from threading import Thread
import paho.mqtt.client as mqttclient

class Mqtt(Thread):

    def __init__(self, callback, topic, host, port):
        super().__init__()
        self.callback = callback
        self.host = host
        self.port = port
        self.topic = topic
        self.daemon = True
        self.cancelled = False

    def receive(self, client, data, msg):
        self.callback(msg.payload.decode("utf-8"))

    def run(self):
        client = mqttclient.Client()
        self.client = client
        client.on_message = self.receive
        client.connect(self.host, self.port, 60)
        client.subscribe(self.topic)
        client.loop_forever()

def mqtt(callback, topic, host, port=1883):
    client = Mqtt(callback, topic, host, port)
    client.start()
    return {
        "publish": client.client.publish
    }
