import regex

word_regex = regex.compile("^[!a-zA-Z0-9-_?.]$")
number = regex.compile("^[0-9]$")
word_start = regex.compile("^[a-zA-Z0-9\_\-]$")
full_number = regex.compile("^(0[box][0-9a-fA-F]*)|(-*([0-9]+_?)[0-9_]*(.[0-9]+)?)$")

class Tokens(object):

    def __init__(self, tokens):
        self.__dict__ = { t:t for t in tokens }

T = Tokens([
    "OPEN_PAREN",
    "CLOSE_PAREN",
    "ID",
    "NUMBER",
    "BOOL",
    "STRING",
    "SQUARE_BR_OPEN",
    "SQUARE_BR_CLOSE",
    "CURLY_BR_OPEN",
    "CURLY_BR_CLOSE",
    "ASSIGN",
    "COLON",
    "NIL",
    "PERCENT",
    "NOT",
    "THREAD",
    "BACKTICK",
    "DOT",
    "DOUBLE_DOT",
    "MULTI_DOT",
    "BINARY_OP",
    "BACKSLASH",
])

LITERALS = {
    "(": T.OPEN_PAREN,
    ")": T.CLOSE_PAREN,
    "[": T.SQUARE_BR_OPEN,
    "]": T.SQUARE_BR_CLOSE,
    "{": T.CURLY_BR_OPEN,
    "}": T.CURLY_BR_CLOSE,
    "=": T.ASSIGN,
    ":": T.COLON,
    "%": T.PERCENT,
    ".": T.DOT,
    "`": T.BACKTICK,
    "+": T.BINARY_OP,
    "*": T.BINARY_OP,
    "/": T.BINARY_OP,
    "<": T.BINARY_OP,
    ">": T.BINARY_OP,
    "\\": T.BACKSLASH,
}

OPERATORS = {
    "-": {
        ">": T.THREAD,
    },
    "=": {
        "=": T.BINARY_OP,
    },
    "!": {
        "=": T.BINARY_OP,
    },
    "<": {
        "=": T.BINARY_OP,
    },
    ">": {
        "=": T.BINARY_OP,
    },
}

class Token:

    def __init__(self, type, value, pos=None):
        self.type = type
        self.value = value
        self.pos = pos or Pos()

    def match(self, *types):
        return self.type in types

    def matchv(self, type, value):
        return self.type == type and self.value==value

    def __str__(self):
        return f"{self.type} '{self.value}'"

class CharStream:

    def __init__(self, src, name=""):
        self.src = list(src)
        self.name = name
        self.line = 1
        self.col = 0
        self._pos = Pos()

    def remember_position(self):
        self._pos = Pos(self.name, self.line, self.col)

    def pos(self, back=0):
        if back:
            return Pos(self.name, self._pos.line, self._pos.col-back+1)
        return self._pos

    def __len__(self):
        return len(self.src)

    def check_next(self):
        return self.src[0]

    def pop(self):
        char = self.src.pop(0)
        self.col += 1
        if char == "\n":
            self.line += 1
            self.col = 0
        return char

class Pos:

    def __init__(self, name="", line=1, col=1):
        self.name = name
        self.line = line
        self.col = col

    def lc(self):
        return f"{self.line}:{self.col}"

    def __str__(self):
        return f"{self.name}: {self.lc()}" if self.name else self.lc()

def lex(source="", name=""):
    tokens = []
    word = ""

    def end_word(src):
        if word:
            if word in [ "true", "false" ]:
                tokens.append(Token(T.BOOL, word, src.pos(len(word))))
            elif full_number.match(word):
                tokens.append(Token(T.NUMBER, word, src.pos(len(word))))
            elif word == "nil":
                tokens.append(Token(T.NIL, word, src.pos(len(word))))
            elif word == "-":
                tokens.append(Token(T.BINARY_OP, word, src.pos(len(word))))
            else:
                if "." in word and not word.startswith("."):
                    for id in word.split("."):
                        tokens.append(Token(T.ID, id, src.pos(len(word))))
                        tokens.append(Token(T.DOT, ".", src.pos(len(word))))
                    tokens.pop()
                else:
                    tokens.append(Token(T.ID, word, src.pos(len(word))))
        return ""

    src = CharStream(source, name)
    while len(src):
        c = src.pop()

        if c in [ ",", " ", "\n" ]:
            word = end_word(src)
            continue
        src.remember_position()

        if c == ".":
            if src:
                if src.check_next() == ".":
                    word = end_word(src)
                    pos = src.pos()
                    src.pop()
                    if src and src.check_next() == ".":
                        src.pop()
                        tokens.append(Token(T.MULTI_DOT, "...", pos))
                    else:
                        tokens.append(Token(T.DOUBLE_DOT, "..", pos))
                elif word_start.match(src.check_next()):
                    word += "."
                else:
                    tokens.append(Token(T.DOT, c, src.pos()))
            else:
                tokens.append(Token(T.DOT, c, src.pos()))
            continue

        if c in OPERATORS:
            if len(src) and src.check_next() in OPERATORS[c]:
                pos = src.pos()
                s = c + src.check_next()
                tokens.append(Token(OPERATORS[c][src.pop()], s, pos))
                continue

        if c == "!" and not word:
            tokens.append(Token(T.NOT, c, src.pos(len(word))))
            continue

        if word_regex.match(c):
            word += c
            continue

        word = end_word(src)

        if c == "#":
            while len(src) and c != "\n":
                c = src.pop()
            continue

        if c == '"':
            s = ""
            n = src.pop()
            while n != '"':
                if n == "\\":
                    lookahead = src.pop()
                    n = "\n" if lookahead == "n" else lookahead
                s += n
                n = src.pop()
            tokens.append(Token(T.STRING, s, src.pos()))
            continue

        if c == "%":
            if len(src) and number.match(src.check_next()):
                c += src.pop()
            tokens.append(Token(T.PERCENT, c, src.pos()))
            continue

        if c in LITERALS:
            tokens.append(Token(LITERALS[c], c, src.pos()))
            continue

        raise Exception(f"unknown character '{c}'")

    word = end_word(src)

    return tokens
