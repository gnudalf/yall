
class YallError(Exception):
    stack = []

class UndefinedError(YallError):
    pass

class SyntaxError(YallError):
    pass

class TestRun:

    def __init__(self,
            name,
            passed,
            pos,
            test_pos,
            func,
            args,
            exception,
        ):
        self.name = name
        self.test_pos = test_pos
        self.pos = pos
        self.passed = passed
        self.func = func
        self.args = args
        self.exception = exception
