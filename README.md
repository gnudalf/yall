# Yet Another Lispy Language

is a functional programming language with lisp syntax written in python.

It is designed to be used
- as an interpretor for small scripts
- via the interactive repl
- directly from python

## Documentation
does not yet exist. But there are some [examples](examples/)

## building, testing
`yall` has been tested with python3.7 and higher.
To build an executable via `pyinstaller`, devel packages need to be installed
or python needs to be compiled with the `--enable-shared` option.

```shell
$ python3 -m venv .venv
$ source .venv/bin/activate
$ pip install -r requirements.txt
$ pip install -r requirements-test.txt
$ ./test.sh
$ pyinstaller -F -n yall main.py
$  
$ yall --help
yall 0.1
    Yet Another Lispy Language

Usage: yall [COMMAND [OPTIONS]] [FILES...]

Commands:
  test FILES...    run tests
  repl FILES...    evaluate files and open repl

Options:
  --help   show this message and exit.
  --vi     select vi mode for repl
```

### docker
```shell
$ docker build -t yall .
```
