from yall.lang.parser import parse, parse_expression
from yall.lang.lexer import lex
from yall.lang.nodes import Env, Func, TestResult as TR
from yall.lang.functions import functions
from yall.lang.common import YallError
import pytest

def pl(s):
    return parse_expression(lex(s))

def test_function_knows_when_it_is_not_recursive():
    env = Env(**functions)
    pl("f = x -> x").eval(env)
    assert not env["f"].recursion

def test_function_knows_when_it_is_recursive():
    env = Env(**functions)
    pl("f = x -> (add 1 (f x))").eval(env)
    assert env["f"].recursion

def test_function_knows_about_recursive_tailcalls():
    env = Env(**functions)
    pl("f = x -> (add 1 (f x))").eval(env)
    assert not len(env["f"].tailcall)
    pl("fn = x -> (fn x)").eval(env)
    assert env["fn"].tailcall

def test_conditional_functions_dont_count_as_tailcalls():
    env = Env(**functions)
    pl("f = x -> (if x (f (sub x 1)) x)").eval(env)
    assert env["f"].tailcall
    assert len(env["f"].tailcall) == 1

def test_simple_tail_call_recursion():
    env = Env(**functions)
    pl("f = x -> (if x (f (sub x 1)) x)").eval(env)
    assert pl("(f 1000)").eval(env) == 0

def test_tail_call_recursion_with_accumulator():
    env = Env(**functions)
    pl(
            "numbers = (n acc=[]) ->" +
            "    (if n (numbers (sub n 1) (cons n acc)) acc)"
    ).eval(env)
    assert len(pl("(numbers 1000)").eval(env)) == 1000

def test_tail_calls_within_nested_conditionals():
    env = Env(**functions)
    pl("f = x -> (if x (or (f (sub x 1))) x)").eval(env)
    assert env["f"].tailcall
    assert pl("(f 1000)").eval(env) == 0

# multiple recursive calls
# renamed tailcall functions
# nested tailcall recursive functions
