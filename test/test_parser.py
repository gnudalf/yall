from yall.lang.parser import parse, parse_expression
from yall.lang.nodes import (
    ConstantNode, RefNode, ApplicationNode, DefinitionNode, LambdaNode, CondNode,
    TestNode as TN, # PytestCollectionWarning: cannot collect test class...
    UseNode, CatchNode, TypedNode
)
from yall.lang.lexer import lex, T
import pytest

def lines(l):
    return "\n".join(l)


def test_nothing():
    with pytest.raises(Exception) as e:
        parse_expression([])
    assert "EOF" in str(e)

def test_number_only():
    node = parse_expression(lex("123"))
    assert isinstance(node, ConstantNode)

def test_bool():
    node = parse_expression(lex("true"))
    assert isinstance(node, ConstantNode)

def test_id_only():
    node = parse_expression(lex("foo"))
    assert isinstance(node, RefNode)

def test_unmatched_open_paren():
    with pytest.raises(Exception) as e:
        parse_expression(lex("(foo"))
    assert "EOF" in str(e)

def test_closed_paren():
    with pytest.raises(Exception) as e:
        parse_expression(lex(")"))
    assert "CLOSE_PAREN" in str(e)

def test_empty_application():
    node = parse_expression(lex("()"))
    assert isinstance(node, ApplicationNode)
    assert len(node) == 0

def test_simple_application():
    node = parse_expression(lex("(add 1 2)"))
    assert isinstance(node, ApplicationNode)
    assert node[0].token.match(T.ID)
    assert node[1].token.match(T.NUMBER)
    assert node[2].token.match(T.NUMBER)
    assert len(node) == 3

def test_nested_application():
    node = parse_expression(lex("(a (b (c 1) 2) 3)"))
    assert isinstance(node, ApplicationNode)
    assert node[0].token.match(T.ID)
    assert isinstance(node[1], ApplicationNode)
    assert isinstance(node[1][1], ApplicationNode)
    assert node[2].token.value == "3"
    assert len(node) == 3

def test_multiple_applications():
    tree = parse(lex(lines([
        "(foo 1 2)",
        "(bar (buzz 3 4) 5)",
    ])))
    assert isinstance(tree[0], ApplicationNode)
    assert tree[0][0].token.value == "foo"
    assert isinstance(tree[1], ApplicationNode)
    assert tree[1][0].token.value == "bar"

def test_definition():
    node = parse_expression(lex("(def a b)"))
    assert isinstance(node, DefinitionNode)
    assert node[0].token.match(T.ID)
    assert node[1].token.match(T.ID)
    assert node[2].token.match(T.ID)
    assert len(node) == 3

def test_lambda():
    node = parse_expression(lex("(lambda (a b) (add a b))"))
    assert isinstance(node, LambdaNode)
    assert node[0].token.match(T.ID)
    assert isinstance(node[1], ApplicationNode)
    assert isinstance(node[2], ApplicationNode)

def test_conditionals():
    node = parse_expression(lex("(if true 1 2)"))
    assert isinstance(node, CondNode)
    assert node[0].token.match(T.ID)
    assert isinstance(node[1], ConstantNode)
    assert isinstance(node[2], ConstantNode)
    assert isinstance(node[3], ConstantNode)

def test_empty_list_with_square_brackets():
    node = parse_expression(lex("[]"))
    assert isinstance(node, ApplicationNode)
    assert node[0].token.value == "list"
    assert len(node) == 1

def test_lists_with_square_brackets():
    node = parse_expression(lex("[ 1 2 3 ]"))
    assert isinstance(node, ApplicationNode)
    assert node[0].token.value == "list"
    assert node[1].token.match(T.NUMBER)
    assert node[2].token.match(T.NUMBER)
    assert node[3].token.match(T.NUMBER)

def test_seq_with_square_brackets():
    node = parse_expression(lex("[ 1 .. 3 ]"))
    assert isinstance(node, ApplicationNode)
    assert node[0].token.value == "seq"
    assert node[1].token.match(T.NUMBER)
    assert node[2].token.match(T.NUMBER)

def test_obj_with_curly_brackets():
    node = parse_expression(lex("{ a:1 }"))
    assert isinstance(node, ApplicationNode)
    assert node[0].token.value == "obj"
    assert len(node) == 2
    assert node[1][0].token.value == "list"
    assert len(node[1]) == 3
    assert node[1][1].token.value == "a"
    assert node[1][1].token.match(T.STRING)

def test_obj_with_references_as_keys():
    node = parse_expression(lex("{ a::1 }"))
    assert isinstance(node, ApplicationNode)
    assert node[0].token.value == "obj"
    assert len(node) == 2
    assert node[1][0].token.value == "list"
    assert len(node[1]) == 3
    assert node[1][1].token.value == "a"
    assert node[1][1].token.match(T.ID)

def test_list_without_closing_square_bracket():
    with pytest.raises(Exception) as e:
        parse_expression(lex("[ 1 "))
    assert "EOF" in str(e)
    with pytest.raises(Exception) as e:
        parse_expression(lex("{ foo:123 "))
    assert "EOF" in str(e)

def test_lambda_with_slash():
    node = parse_expression(lex("/(a b): (or a b)"))

def test_test_statememt():
    node = parse_expression(lex('(test true)'))
    assert isinstance(node, TN)

def test_use_statememt():
    node = parse_expression(lex('(use "foo")'))
    assert isinstance(node, UseNode)
    node = parse_expression(lex('(use foo)'))
    assert isinstance(node, UseNode)

def test_catch_statememt():
    node = parse_expression(lex('(catch (error) ()->false)'))
    assert isinstance(node, CatchNode)

def test_assignment_statememt():
    node = parse_expression(lex('foo = 42'))
    assert isinstance(node, DefinitionNode)

def test_nil():
    node = parse_expression(lex("nil"))
    assert isinstance(node, ConstantNode)

def test_percent_lambdas():
    node = parse_expression(lex("(eq % 1)"))
    assert isinstance(node, LambdaNode)
    assert len(node.params) == 1

def test_not_syntax():
    node = parse_expression(lex("! true"))
    assert isinstance(node, ApplicationNode)
    assert len(node) == 2
    assert node[0].token.value == "not"
    assert node[1].token.match(T.BOOL)

def test_threading_macro():
    node = parse_expression(lex("(-> 1 (add 2) (sub 1))"))
    assert isinstance(node, ApplicationNode)
    assert len(node) == 3
    assert node[0].token.value == "sub"

def test_caaaddadar_expansion():
    node = parse_expression(lex("(cadadadr x)"))
    assert isinstance(node, ApplicationNode)
    assert len(node) == 2
    assert node[0].token.value == "car"
    assert node[1][0].token.value == "cdr"
    assert node[1][1][0].token.value == "car"
    assert node[1][1][1][0].token.value == "cdr"
    assert node[1][1][1][1][0].token.value == "car"
    assert node[1][1][1][1][1][0].token.value == "cdr"
    assert node[1][1][1][1][1][1].token.value == "x"

def test_caaaddadar_expansion_as_references():
    node = parse_expression(lex("(foo caddr x)"))
    assert isinstance(node, ApplicationNode)
    assert isinstance(node[1], LambdaNode)
    assert node[1][2][0].token.value == "car"
    assert node[1][2][1][0].token.value == "cdr"
    assert node[1][2][1][1][0].token.value == "cdr"

def test_dot_syntax():
    node = parse_expression(lex("a . b"))
    assert isinstance(node, ApplicationNode)
    assert len(node) == 3
    assert node[0].token.value == "get"
    assert node[1].token.value == "a"
    assert node[2].token.value == "b"

def test_leading_dot():
    node = parse_expression(lex(".env"))
    assert isinstance(node, RefNode)
    assert node.token.value == ".env"

def test_nested_dots():
    node = parse_expression(lex("{} . b . c . d"))
    assert node[0].token.value == "get"
    assert node[1][0].token.value == "get"
    assert node[1][1][0].token.value == "get"
    assert node[1][1][1][0].token.value == "obj"
    assert node[1][1][2].token.value == "b"
    assert node[1][1][2].token.match(T.STRING)
    assert node[1][2].token.value == "c"
    assert node[1][2].token.match(T.STRING)
    assert node[2].token.value == "d"
    assert node[2].token.match(T.STRING)

def test_infix_expressions():
    node = parse_expression(lex("17 `mod` 10"))
    assert isinstance(node, ApplicationNode)
    assert node[0].token.value == "mod"
    assert node[1].token.value == "17"
    assert node[2].token.value == "10"

def test_implicit_infix_lambda():
    node = parse_expression(lex("% `divisible?` 2"))
    assert isinstance(node, LambdaNode)

def test_multiple_multiarg_lambdas_dont_work():
    with pytest.raises(Exception) as e:
        parse_expression(lex("(lambda (a b... c...) (len b))"))
    assert "MULTI" in str(e)

def test_multiarg_lambda():
    node = parse_expression(lex("(lambda (a b...) (len b))"))
    assert isinstance(node, LambdaNode)

def test_function_composition_of_expressions():
    node = parse_expression(lex("f..(+ 1)"))
    assert isinstance(node, LambdaNode)

def test_function_composition_as_reference():
    node = parse_expression(lex("f..g"))
    assert isinstance(node, LambdaNode)
    node = parse_expression(lex("(map foo f..g)"))
    assert isinstance(node[2], LambdaNode)

def test_direct_application_of_function_composition():
    node = parse_expression(lex("(f..g 1)"))
    #(f (g 1))
    assert isinstance(node, ApplicationNode)
    assert isinstance(node[0], RefNode)

def test_multiple_function_compositions():
    node = parse_expression(lex("a..b..c"))
    assert isinstance(node, LambdaNode)
    assert node[2][0].token.value == "a"
    assert node[2][1][0].token.value == "b"
    assert node[2][1][1][0].token.value == "c"
    node = parse_expression(lex("(a..b..c 1)"))
    assert isinstance(node, ApplicationNode)
    assert node[0].token.value == "a"
    assert node[1][0].token.value == "b"
    assert node[1][1][0].token.value == "c"

def test_application_of_binary_operators():
    node = parse_expression(lex("(+ 1 2)"))
    assert isinstance(node, ApplicationNode)
    assert isinstance(node[0], RefNode)
    node = parse_expression(lex("(/ (a) 2)"))
    assert isinstance(node, ApplicationNode)
    assert len(node) == 3
    assert isinstance(node[0], RefNode)
    assert isinstance(node[1], ApplicationNode)
    assert isinstance(node[2], ConstantNode)

def test_binary_operator_infix_syntax():
    node = parse_expression(lex("1 + 2"))
    assert isinstance(node, ApplicationNode)
    assert isinstance(node[0], RefNode)
    assert isinstance(node[1], ConstantNode)
    assert isinstance(node[2], ConstantNode)

def test_nested_binary_operator_infix_syntax():
    node = parse_expression(lex("1 + 2 + 3"))
    assert isinstance(node, ApplicationNode)
    assert str(node) == "(+ (+ 1 2) 3)"

def test_precendence_of_nested_binary_operator_infix():
    node = parse_expression(lex("1 * 2 + 3"))
    assert str(node) == "(+ (* 1 2) 3)"
    node = parse_expression(lex("1 + 2 * 3 - 4"))
    assert str(node) == "(- (+ 1 (* 2 3)) 4)"

def test_infix_operators_get_reordered_in_application():
    node = parse_expression(lex("(3 - 2) * 1"))
    assert str(node) == "(* (- 3 2) 1)"

def test_simple_cond():
    node = parse_expression(lex("(cond ((foo x) 0))"))
    assert isinstance(node, CondNode)
    assert str(node[0]) == "if"

def test_nested_cond():
    node = parse_expression(lex("(cond ((foo x) 0) ((bar x) 1) (else 2))"))
    assert isinstance(node, CondNode)
    assert node[2].token.value == "0"
    assert node[3][2].token.value == "1"
    assert node[3][3][2].token.value == "2"

def test_new_lambda_syntax():
    node = parse_expression(lex("(a b) -> [a b]"))
    assert isinstance(node, LambdaNode)
    assert node[1][0].token.value == "a"
    assert node[1][1].token.value == "b"
    assert node[2][0].token.value == "list"

def test_new_lambda_single_arg_syntax():
    node = parse_expression(lex("a -> [a]"))
    assert isinstance(node, LambdaNode)
    assert node[1][0].token.value == "a"
    assert node[2][0].token.value == "list"

def test_new_lambda_list_arg():
    node = parse_expression(lex("([a b]) -> a + b"))
    assert isinstance(node, LambdaNode)
    assert node[1][0].token.value == "%1"
    assert isinstance(node[1][1], DefinitionNode)
    assert isinstance(node[1][2], DefinitionNode)
    assert node[1][1][1].token.value == "a"
    assert node[1][1][2][0].token.value == "car"
    assert node[1][1][2][1].token.value == "%1"
    assert node[1][2][1].token.value == "b"
    assert node[1][2][2][0].token.value == "car"
    assert node[1][2][2][1][0].token.value == "cdr"
    assert node[1][2][2][1][1].token.value == "%1"
    assert node[2][0].token.value == "+"
    assert node[2][1].token.value == "a"
    assert node[2][2].token.value == "b"

def test_new_lambda_with_one_default_arg():
    node = parse_expression(lex("(a=1) -> a"))
    assert isinstance(node, LambdaNode)
    assert isinstance(node[1][0], DefinitionNode)
    assert node[2].token.value == "a"

def test_variadic_new_lambda():
    node = parse_expression(lex("(a...) -> a"))
    assert isinstance(node, LambdaNode)
    assert node[1][0].token.value == "a"
    assert node[1][0].multiarg
    assert node[2].token.value == "a"

def test_new_lambda_without_args():
    node = parse_expression(lex("() -> 42"))
    assert isinstance(node, LambdaNode)
    assert len(node[1]) == 0
    assert node[2].token.value == "42"

def test_expression_with_type_annotations():
    node = parse_expression(lex("foo : int"))
    assert isinstance(node, TypedNode)
    assert str(node.exp()) == "foo"
    assert str(node.type()) == "int"

def test_lambda_with_type_annotations():
    node = parse_expression(lex("(a:int b:str) -> true"))
    assert isinstance(node, LambdaNode)
    assert "a" in node.params
    assert "b" in node.params
