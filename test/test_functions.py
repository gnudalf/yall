from yall.lang.functions import functions, format_yall
from yall.lang.nodes import Env
import pytest
import regex
from datetime import datetime

def test_add():
    assert functions.add(1, 2) == 3
    assert functions["add"](1, 2, 3, 4) == 10

def test_sub():
    assert functions.sub(2, 1) == 1
    assert functions.sub(5, 2, 1) == 2

def test_mul():
    assert functions.mul(2, 3) == 6
    assert functions.mul(2, 3, 4) == 24

def test_div():
    assert functions.div(10, 2) == 5
    assert functions.div(80, 2, 2, 2) == 10

def test_mod():
    assert functions.mod(10, 2) == 0
    assert functions.mod(4, 3) == 1
    assert functions.mod(50, 10, 5) == 0

def test_not():
    assert not functions["not"](True)
    assert functions["not"](False)
    assert functions["not"](False, 0, "")
    assert not functions["not"](1, [1], "foo")

def test_is():
    assert functions["is?"](10, 10)
    assert functions["is?"](True, True)
    assert functions["is?"](None, None)
    assert not functions["is?"](10, 9)
    assert not functions["is?"](False, True)
    assert not functions["is?"]([], [])

def test_eq():
    assert functions.eq(10, 10)
    assert not functions.eq(10, 10, 9)

def test_ne():
    assert not functions.ne(10, 10)
    assert not functions.ne(10, 10, 9)
    assert functions.ne(10, 9, 8)

def test_gt():
    assert not functions.gt(1, 1)
    assert functions.gt(2, 1)
    assert functions.gt(4, 3, 2, 1)
    assert not functions.gt(4, 3, 2, 2)

def test_ge():
    assert not functions.ge(0, 1)
    assert functions.ge(1, 1)
    assert functions.ge(2, 1)
    assert functions.ge(4, 3, 2, 2)
    assert not functions.ge(4, 3, 2, 3)

def test_lt():
    assert functions.lt(1, 2)
    assert not functions.lt(1, 1)
    assert functions.lt(1, 2, 3, 4)
    assert not functions.lt(1, 2, 3, 3)

def test_le():
    assert functions.le(1, 2)
    assert functions.le(1, 1)
    assert not functions.le(2, 1)
    assert functions.le(1, 2, 2, 4)
    assert not functions.le(1, 2, 2, 1)

def test_seq():
    assert functions.seq(3) == [ 1, 2, 3 ]
    assert functions.seq(3, 6) == [ 3, 4, 5, 6 ]

def test_format():
    assert format_yall(3) == "3"
    assert format_yall(1.2) == "1.2"
    assert format_yall(0) == "0"
    assert format_yall("foo") == '"foo"'
    assert format_yall(True) == "true"
    assert format_yall(False) == "false"
    assert format_yall([ 0, True, "asdf" ]) == '[0 true "asdf"]'
    assert format_yall({ "foo": 123, "bar": 456 }) == '{"foo":123 "bar":456}'
    assert format_yall(None) == "nil"

def test_native_function_format():
    env = Env(**functions)
    assert format_yall(env["list"]) == "𝝺 list"
    assert format_yall(env["map"]) == "𝝺 map"
    assert format_yall(env["substr"]) == "𝝺 substr"

def test_format_datetime():
    env = Env(**functions)
    date = regex.compile("^[0-9]{4}-[0-9]{2}-[0-9]{2}")
    assert date.match(format_yall(datetime.now()))
