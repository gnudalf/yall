from yall.lang.parser import parse_expression
from yall.lang.lexer import lex
from yall.lang.nodes import Env
from yall.lang.functions import functions, format_yall, Suspension

def pl(s):
    return parse_expression(lex(s))

def test_conss():
    env = Env(**functions)
    result = pl("(conss 1 [2 3])").eval(env)
    assert isinstance(result, Suspension)

def test_car_of_conss():
    env = Env(**functions)
    pl("l = (conss 1 (cons 2 (cons 3 nil)))").eval(env)
    assert pl("(car l)").eval(env) == 1

def test_cdr_of_conss():
    env = Env(**functions)
    pl("l = (conss 1 (conss 2 (conss 3 nil)))").eval(env)
    assert isinstance(pl("(cdr l)").eval(env), Suspension)
    assert isinstance(pl("(cdr (cdr l))").eval(env), Suspension)
    assert pl("(cdr (cdr (cdr l)))").eval(env) == []

def test_cadr_of_conss():
    env = Env(**functions)
    pl("l = (conss 1 (conss 2 (conss 3 nil)))").eval(env)
    assert pl("(car (cdr l))").eval(env) == 2

def test_cadr_of_conss():
    env = Env(**functions)
    pl("l = (conss 1 (conss 2 (conss 3 nil)))").eval(env)
    assert pl("(cadr l)").eval(env) == 2

def test_bool_of_conss():
    env = Env(**functions)
    pl("l = (conss 1 (conss 2 nil))").eval(env)
    assert pl("(bool (cdr l))").eval(env)
    assert not pl("(bool (cdr..cdr l))").eval(env)

def test_str_of_conss():
    env = Env(**functions)
    pl("l = (conss 1 (conss 2 (conss 3 nil)))").eval(env)
    assert format_yall(env["l"]) == "[1 2 3]"

def test_conss_does_not_evaluate_arguments():
    env = Env(**functions)
    pl('l = (conss 1 (conss (error "foo") (conss 3 nil)))').eval(env)
    assert pl('(caddr l)').eval(env) == 3

def test_conss_on_lists():
    env = Env(**functions)
    pl('l = (conss 1 (list 2 3))').eval(env)
    assert env["l"].to_list() == [1, 2, 3]

def test_suspension_can_be_converted_to_list():
    env = Env(**functions)
    pl('l = (conss 1 (conss 2 (conss 3 nil)))').eval(env)
    assert env["l"].to_list() == [1,2,3]

def test_suspension_can_be_compared_to_list():
    env = Env(**functions)
    pl('l = (conss 1 (conss 2 (conss 3 nil)))').eval(env)
    assert pl('(== l [1 2 3])').eval(env)

def test_two_suspensions_are_not_fully_evaluated_when_compared():
    env = Env(**functions)
    pl('l1 = (conss 1 (conss 2 (conss 3 nil)))').eval(env)
    pl('l2 = (conss 1 (conss 2 (conss 3 nil)))').eval(env)
    pl('l3 = (conss 4 (conss (error "nope") (conss 3 nil)))').eval(env)
    assert pl('(== l1 l2)').eval(env)
    assert not pl('(== l1 l3)').eval(env)

def test_two_suspensions_are_suicidal():
    env = Env(**functions)
    env.update({ "a": 1 })
    pl('l = (conss a (conss 2 nil))').eval(env)
    assert pl('l').eval(env).to_list() == [1, 2]
    env.update({ "a": 42 })
    assert pl('l').eval(env).to_list() == [1, 2]

def test_car_cdr_can_be_referenced():
    env = Env(**functions)
    pl('f = (fn x) -> (fn x)').eval(env)
    assert pl('(f car [1 2])').eval(env) == 1
    assert pl('(f cdr [1 2])').eval(env) == [2]

def test_conss_assumes_nil_as_default():
    env = Env(**functions)
    assert pl('(conss 1)').eval(env).to_list() == [1]

def test_apply_str_on_conss():
    env = Env(**functions)
    assert pl('(apply str (conss "a" (conss "b")))').eval(env) == "ab"

def test_conss_to_list():
    env = Env(**functions)
    assert pl('(tolist (conss 1 (conss 2)))').eval(env) == [1, 2]

def test_conss_is_a_list():
    env = Env(**functions)
    assert pl('(list? (conss 1 (conss 2)))').eval(env)
