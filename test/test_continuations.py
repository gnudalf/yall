from yall.lang.parser import parse_expression
from yall.lang.lexer import lex
from yall.lang.nodes import Env
from yall.lang.functions import functions
import pytest

def pl(s):
    return parse_expression(lex(s))

def test_nested_continuation_functions_dont_blow_the_stack():
    env = Env(**functions)
    n = pl(
        "fact-cps = (n k) ->" + \
        "    (if (zero? n) (k 1)" + \
        "        (fact-cps (- n 1) v -> (k (* v n))))"
    )
    n.eval(env)
    assert pl("(fact-cps 5 x -> x)").eval(env) == 120
    assert pl("(fact-cps 500 x -> x)").eval(env)

def test_simple_cps_transformation():
    env = Env(**functions)
    n = pl("fact = (n) -> (if (zero? n) 1 (* n (fact (- n 1))))")
    n.eval(env)
    assert pl("(fact 1000)").eval(env)

def test_simple_map():
    env = Env(**functions)
    n = pl(
        "my-map = (f l) ->" + \
        "    (if !l l ((cons % %) (f (car l)) (my-map f (cdr l))))"
    )
    n.eval(env)
    assert pl("(my-map (+ 1) (seq 1000))").eval(env) == list(range(2, 1002))
