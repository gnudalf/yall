from yall.lang.magic import magic
import pytest
import os
from datetime import datetime

def test_env():
    assert type(magic["env!"]()) == dict

def test_argv():
    assert type(magic["argv!"]()) == list

def test_random():
    assert type(magic["rand!"]()) == float
    assert magic["rand!"]() != magic["rand!"]()

def test_int_random():
    assert type(magic["rand!"](100)) == int

def test_random_list():
    l = [ 1, 2, 3, 4, 5 ]
    for i in range(20):
        assert magic["rand!"](l) in l

def test_uuid():
    assert magic["uuid!"]() != magic["uuid!"]()

def test_json_decode():
    assert magic["json!-decode"]('{ "foo": 123 }') == { "foo": 123 }

def test_json_encode():
    assert magic["json!-encode"]({ "foo": 123 }) == '{"foo": 123}'

def test_system():
    result = magic["system!"]("pwd")
    assert result["exitcode"] == 0
    assert result["stdout"].strip() == os.environ.get("PWD")

def test_now():
    result = magic["now!"]()
    assert type(result) == datetime

def test_date_from_ints():
    result = magic["date!"](2024,12,3,4,56,7,8)
    assert type(result) == datetime
    assert result.year == 2024
    assert result.month == 12
    assert result.day == 3
    assert result.hour == 4
    assert result.minute == 56
    assert result.second == 7
    assert result.microsecond == 8
    result = magic["date!"](2024,1,1)
    assert result.year == 2024
    assert result.month == 1
    assert result.day == 1
    assert result.hour == 0
    assert result.minute == 0
    assert result.second == 0
    assert result.microsecond == 0

def test_date_from_string():
    result = magic["date!"]("2024-12-03T04:56:07.123456")
    assert result.year == 2024
    assert result.month == 12
    assert result.day == 3
    assert result.hour == 4
    assert result.minute == 56
    assert result.second == 7
    assert result.microsecond == 123456

def test_timestamp():
    time = magic["date!"](2024,12,3,4,56,7,123456)
    result = magic["timestamp!"](time)
    assert result == 1733201767.123456
    result = magic["timestamp!"](1733201767.123456)
    assert result == time
