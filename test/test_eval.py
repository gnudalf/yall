from yall.lang.parser import parse, parse_expression
from yall.lang.lexer import lex
from yall.lang.nodes import Env, Func, TestResult as TR, Node
from yall.lang.functions import functions
from yall.lang.common import YallError
from datetime import datetime
import pytest

add_func = lambda x, y: x + y
mul_func = lambda x, y: x * y

def pl(s):
    return parse_expression(lex(s))

def test_env():
    env = Env(**{
        "foo": "bar",
        "bar": 42
    })
    assert env["foo"] == "bar"
    assert env["bar"] == 42

def test_number_only():
    assert pl("123").eval(None) == 123
    assert pl("0.5").eval(None) == 0.5

def test_negatives_and_double_negatives():
    assert pl("-42.0").eval(Env()) == -42.0
    assert pl("--42").eval(Env()) == 42

def test_bools():
    assert pl("true").eval(None)
    assert not pl("false").eval(None)

def test_strings():
    assert pl('"foo bar"').eval(None) == "foo bar"

def test_strings_with_newline():
    assert pl('"foo\\nbar"').eval(None) == "foo\nbar"

def test_reference():
    node = pl("foo")
    env = Env(**{
        "foo": "bar",
    })
    assert node.eval(env) == "bar"

def test_application():
    node = pl("(add one 2)")
    env = Env(**{
        "one": 1,
        "add": add_func
    })
    assert node.eval(env) == 3

def test_empty_application():
    node = pl("()")
    assert node.eval(Env()) is None

def test_definition():
    node = pl("(def foo 42)")
    env = Env(bar="test")
    node.eval(env)
    assert env["foo"] == 42
    assert env["bar"] == "test"

def test_definition_needs_a_name():
    with pytest.raises(Exception) as e:
        pl("(def)")
    assert "name" in str(e)

    with pytest.raises(Exception) as e:
        pl("(def 42 123)")
    assert "42" in str(e)

def test_definition_needs_a_value():
    with pytest.raises(Exception) as e:
        pl("(def foo)")
    assert "missing value" in str(e) and "foo" in str(e)

def test_definition_must_not_have_extra_args():
    with pytest.raises(Exception) as e:
        pl("(def foo bar nope)")
    assert "extra" in str(e) and "foo" in str(e)

def test_definition_evaluates():
    assert pl("(def foo 42)").eval(Env()) == 42

def test_lambda():
    node = pl("(lambda (a) (add 1 a))")
    env = Env(
        add=add_func,
    )
    func = node.eval(env)
    assert isinstance(func, Func)
    assert func(41) == 42

def test_defined_lambda():
    env = Env(add=add_func)
    pl("(def inc (lambda (a) (add 1 a)))").eval(env)
    assert pl("(inc 41)").eval(env) == 42
    assert "inc" in env
    assert "add" in env
    assert "a" not in env

def test_direct_application():
    env = Env(add=add_func)
    node = pl("((lambda (a) (add 1 a)) 41)")
    assert node.eval(env) == 42

def test_closure():
    env = Env(add=add_func)
    node = pl("(def inc (lambda (by) (lambda (n) (add n by))))")
    node.eval(env)
    assert "inc" in env
    pl("(def inc2 (inc 2))").eval(env)
    assert "inc2" in env
    assert pl("(inc2 40)").eval(env) == 42

def test_function_as_reference():
    env = Env(add=add_func, mul=mul_func)
    pl("(def change-21 (lambda (f x) (f 21 x)))").eval(env)
    assert "change-21" in env
    assert pl("(change-21 add 21)").eval(env) == 42
    assert pl("(change-21 mul 2)").eval(env) == 42
    assert pl("(change-21 (lambda (a b) 123) 0)").eval(env) == 123

def test_lambda_with_no_arguments():
    assert pl("((lambda 42))").eval(Env()) == 42

def test_lambda_must_not_have_extra_arguments():
    with pytest.raises(Exception) as e:
        pl("(lambda (a) 42 nope)")
    assert "extra" in str(e)

def test_lambda_must_have_an_expression():
    with pytest.raises(Exception) as e:
        pl("(lambda)")
    assert "expression" in str(e)

def test_if():
    env = Env(**functions)
    node = pl("\n".join([
        '(def what-is ' +
        '   (lambda (n) ' +
        '       (if (eq n 42) "the answer" "nope")))'
    ])).eval(env)
    assert pl("(what-is 42)").eval(env) == "the answer"
    assert pl("(what-is 41)").eval(env) == "nope"

    assert pl("(if true 1 (div 10 0))").eval(env)
    with pytest.raises(Exception):
        pl("(if false 1 (div 10 0))").eval(env)

def test_if_nil_default():
    env = Env(**functions)
    assert pl("(if true 1)").eval(env) == 1
    assert pl("(if false 1)").eval(env) is None

def test_and():
    assert pl("(and true 42)").eval(Env(**functions)) == 42
    assert not pl("(and 4 0 (div 1 0))").eval(Env(**functions))

def test_or():
    assert pl("(or 123 42 (div 1 0))").eval(Env(**functions)) == 123
    assert pl("(or 0 false 2)").eval(Env(**functions)) == 2
    assert not pl("(or 0 false)").eval(Env(**functions))

def test_and_or_as_referenced():
    env = Env(**functions)
    pl('f=(fn a b) -> (fn a b)').eval(env)
    assert pl("(f and 1 2)").eval(env) == 2
    assert pl("(f and 0 2)").eval(env) == 0

def test_recursion():
    env = Env(**functions)
    pl("(def fact (lambda (n) (if (gt n 1) (mul n (fact (sub n 1))) 1)))").eval(env)
    assert pl("(fact 2)").eval(env) == 2
    assert pl("(fact 7)").eval(env) == 5040

def test_lists():
    env = Env(**functions)
    assert pl("(list 1 2 3)").eval(env) == [ 1, 2, 3 ]
    pl("(def l (list 1 2 3 4))").eval(env)

def test_all_cadaddaar_variants():
    env = Env(**functions)
    pl("(def l (list 1 2 3 4))").eval(env)
    assert pl("(car l)").eval(env) == 1
    assert pl("(cdr l)").eval(env) == [ 2, 3, 4 ]
    assert pl("(cadr l)").eval(env) == 2
    assert pl("(cddr l)").eval(env) == [ 3, 4 ]
    assert pl("(caar [ [ 1 2 ] [ 3 ] 4 5 ])").eval(env) == 1
    assert pl("(cadar [ [ 1 2 ] [ 3 ] 4 5 ])").eval(env) == 2
    assert pl("(cdar [ [ 1 2 3 ] 4 5 ])").eval(env) == [2, 3]
    assert pl("(cadaddr [ [ 1 2 ] [ 3 4 ] [ 5 6 ] ])").eval(env) == 6

def test_all_cars_work_on_objects():
    env = Env(**functions)
    pl('o = { 1:2 3:4 5:6 }').eval(env)
    assert pl("(car o)").eval(env) == [1, 2]
    assert pl("(cadr o)").eval(env) == [3, 4]
    assert pl("(cadar o)").eval(env) == 2

def test_lists_of_lists():
    env = Env(**functions)
    assert pl("(list (list 1 2) (list 3 4))").eval(env) == [[1,2], [3,4]]

def test_lists_of_exactly_one_list():
    env = Env(**functions)
    assert pl("[[]]").eval(env) == [[]]
    assert pl("[[1]]").eval(env) == [[1]]

def test_lists_of_objs():
    env = Env(**functions)
    assert pl("[ (obj) (obj) ]").eval(env) == [ {}, {} ]

def test_length():
    env = Env(**functions)
    assert pl("(len (seq 4))").eval(env) == 4
    assert pl("(len (list))").eval(env) == 0
    assert pl('(len "")').eval(env) == 0
    assert pl('(len "foobar")').eval(env) == 6
    assert pl('(len (obj))').eval(env) == 0
    assert pl('(len (obj (list "foo" "bar")))').eval(env) == 1

def test_map():
    env = Env(**functions)
    pl("(def square (lambda (n) (mul n n)))").eval(env)
    assert pl("(map square (seq 4))").eval(env) == [ 1, 4, 9, 16 ]

def test_map_to_constant():
    env = Env(**functions)
    assert pl("(map 2 (seq 4))").eval(env) == [2,2,2,2]

def test_filter():
    env = Env(**functions)
    pl("(def l (seq 8))").eval(env)
    pl("(def even (lambda (n) (eq (mod n 2) 0)))").eval(env)
    assert pl("(filter even l)").eval(env) == [ 2, 4, 6, 8 ]

def test_filter_default():
    env = Env(**functions)
    result = pl('(filter bool [true false "a" "" 3 0 [1] []])').eval(env)
    assert result == [ True, "a", 3, [1] ]

def test_object():
    env = Env(**functions)
    pl('(def some-name "bar")').eval(env)
    assert pl('(obj (list "foo" 12) (list some-name 34))').eval(env) == \
            { "foo": 12, "bar": 34 }

def test_obj_to_list():
    env = Env(**functions)
    pl('(def o (obj (list "foo" 12) (list "bar" 34)))').eval(env)
    assert pl('(list o)').eval(env) == \
            [ ["foo", 12], ["bar", 34] ]

def test_obj_map():
    env = Env(**functions)
    pl('(def o (obj (list "foo" 12) (list "bar" 34)))').eval(env)
    assert pl('(map (car %) o)').eval(env) == \
            [ "foo", "bar" ]

def test_obj_filter():
    env = Env(**functions)
    pl('(def o (obj (list "foo" 12) (list "bar" 34)))').eval(env)
    assert pl('(filter (lambda (v) (gt (cadr v) 15)) o )').eval(env) == \
            [ [ "bar", 34 ] ]

def test_has_list():
    env = Env(**functions)
    assert pl('(has? (seq 10) 5)').eval(env)
    assert not pl('(has? (seq 10) 15)').eval(env)

def test_has_obj():
    env = Env(**functions)
    assert pl('(has? (obj (list "foo" 123)) "foo")').eval(env)
    assert not pl('(has? (obj (list "foo" 123)) "bar")').eval(env)

def test_has_string():
    env = Env(**functions)
    assert pl('(has? "this contains a foo and bar" "foo")').eval(env)
    assert not pl('(has? "this contains a foo and bar" "nope")').eval(env)

def test_list_get():
    env = Env(**functions)
    assert pl('(get (list 2 4 8) 0)').eval(env) == 2
    assert pl('(get (list 2 4 8) 2)').eval(env) == 8
    with pytest.raises(Exception) as e:
        pl('(get (list) 0)').eval(env)
    assert "0" in str(e)

def test_obj_get():
    env = Env(**functions)
    pl('(def o (obj (list "foo" 12) (list "bar" 34)))').eval(env)
    assert pl('(get o "foo")').eval(env) == 12
    with pytest.raises(Exception) as e:
        pl('(get o "nope")').eval(env)
    assert "nope" in str(e)

def test_int_function():
    env = Env(**functions)
    assert pl('(int 42)').eval(env) == 42
    assert pl('(int 42.5)').eval(env) == 42
    assert pl('(int "42")').eval(env) == 42

def test_float_function():
    env = Env(**functions)
    assert pl('(float 42)').eval(env) == 42.0
    assert pl('(float 42.5)').eval(env) == 42.5
    assert pl('(float "42")').eval(env) == 42.0

def test_bool_function():
    env = Env(**functions)
    assert pl('(bool "")').eval(env) is False
    assert pl('(bool "123")').eval(env) is True
    assert pl('(bool 0)').eval(env) is False
    assert pl('(bool 2)').eval(env) is True

def test_is_number_function():
    env = Env(**functions)
    assert pl('(number? 42)').eval(env)
    assert pl('(number? 12.34)').eval(env)
    assert not pl('(number? "42")').eval(env)
    assert not pl('(number? (list))').eval(env)

def test_is_bool_function():
    env = Env(**functions)
    assert not pl('(bool? "")').eval(env)
    assert not pl('(bool? 0)').eval(env)
    assert pl('(bool? true)').eval(env)
    assert pl('(bool? false)').eval(env)

def test_is_float_function():
    env = Env(**functions)
    assert pl('(float? 12.34)').eval(env)
    assert not pl('(float? 42)').eval(env)
    assert not pl('(float? (list))').eval(env)

def test_is_string_function():
    env = Env(**functions)
    assert pl('(str? "foo bar")').eval(env)
    assert pl('(str? "42")').eval(env)
    assert not pl('(str? 42)').eval(env)
    assert not pl('(str? (list))').eval(env)

def test_is_list_function():
    env = Env(**functions)
    assert pl('(list? [])').eval(env)
    assert pl('(list? (list 1 2 3))').eval(env)
    assert not pl('(list? "foo bar")').eval(env)
    assert not pl('(list? (obj (list "foo" 123)))').eval(env)

def test_is_obj_function():
    env = Env(**functions)
    assert pl('(obj? (obj))').eval(env)
    assert pl('(obj? (obj (list "foo" 123)))').eval(env)
    assert not pl('(obj? (list 1 2 3))').eval(env)
    assert not pl('(obj? "foo bar")').eval(env)

def test_is_zero():
    env = Env(**functions)
    assert pl('(zero? 0)').eval(env)
    assert pl('(zero? 0.0)').eval(env)
    assert not pl('(zero? false)').eval(env)
    assert not pl('(zero? [])').eval(env)
    assert not pl('(zero? 0.1)').eval(env)
    assert not pl('(zero? true)').eval(env)

def test_list_syntactic_sugar():
    env = Env(**functions)
    assert pl('(eq (list 1 2 3) [ 1 2 3 ])').eval(env)
    assert not pl('(eq (list 1 2 3) [ 1 2 ])').eval(env)

def test_obj_syntactic_sugar():
    env = Env(**functions)
    pl('(def name "foo")').eval(env)
    obj = pl('{ (()-> name):123 "bar": 456 }').eval(env)
    assert obj == { "foo": 123, "bar": 456, }

def test_curly_obj_syntactic_sugar():
    env = Env(**functions)
    pl('(def name "foo")').eval(env)
    obj = pl('{ name:123 "bar": 456 }').eval(env)
    assert obj == { "name": 123, "bar": 456, }

def test_string_conversion():
    env = Env(**functions)
    assert pl('(str "foo")').eval(env) == 'foo'
    assert pl('(str 0123)').eval(env) == "123"
    assert pl('(str { "foo":123 "bar":456 })').eval(env) == \
        '{"foo":123 "bar":456}'

def test_string_conconcatenation():
    env = Env(**functions)
    assert pl('(str "foo" "bar")').eval(env) == 'foobar'

def test_multidefinition_fails():
    env = Env()
    pl('(def foo 123)').eval(env)
    assert pl('foo').eval(env) == 123
    with pytest.raises(Exception) as e:
        pl('(def foo 123)').eval(env)
    assert "foo" in str(e)

def test_reusing_variable_names_in_child_scope():
    env = Env(**functions)
    pl('(def foo 123)').eval(env)
    assert pl('((lambda (foo) (add foo 1)) 41)').eval(env) == 42

def test_slash_lambda():
    env = Env(**functions)
    pl('(def inc (a b) -> (add a b))').eval(env)
    assert pl('(inc 40 2)').eval(env) == 42

def test_slash_lambda_with_single_arg():
    env = Env(**functions)
    assert pl('(a ->(add a 1) 41)').eval(env) == 42

def test_slash_lambda_without_args():
    env = Env(**functions)
    assert pl('(()->42)').eval(env) == 42

def test_list_mutating_cons():
    env = Env(**functions)
    assert pl('(cons! 1 [ 2 3 ])').eval(env) == [ 1,2,3 ]
    pl('l = (seq 3)').eval(env)
    assert pl('(cons! 4 l)').eval(env) == [ 4, 1,2,3 ]
    assert pl('l').eval(env) == [ 4, 1,2,3 ]

def test_obj_mutating_cons():
    env = Env(**functions)
    assert pl('(cons! { "a":1 } { "b":2 })').eval(env) == { "a": 1, "b": 2 }
    pl('o = { "b": 2 }').eval(env)
    assert pl('(cons! { "a":1 } o)').eval(env) == { "a": 1, "b": 2 }
    assert pl('o').eval(env) == { "a": 1, "b": 2 }

def test_list_cons():
    env = Env(**functions)
    assert pl('(cons 1 [ 2 3 ])').eval(env) == [ 1,2,3 ]
    pl('l = (seq 3)').eval(env)
    assert pl('(cons 4 l)').eval(env) == [ 4, 1,2,3 ]
    assert pl('l').eval(env) == [ 1,2,3 ]
    assert pl('(cons 1 nil)').eval(env) == [1]
    assert pl('(cons 1)').eval(env) == [1]

def test_obj_cons():
    env = Env(**functions)
    assert pl('(cons { "a":1 } { "b":2 })').eval(env) == { "a": 1, "b": 2 }
    pl('o = { "b": 2 }').eval(env)
    assert pl('(cons { "a":1 } o)').eval(env) == { "a": 1, "b": 2 }
    assert pl('o').eval(env) == { "b": 2 }

def test_str_cons():
    env = Env(**functions)
    assert pl('(cons "foo" "bar")').eval(env) == "foobar"

def test_cons_error():
    env = Env(**functions)
    with pytest.raises(Exception) as e:
        pl('(cons "foo" 123)').eval(env)

def test_list_append():
    env = Env(**functions)
    assert pl('(append! [ 1 2 ] 3 4)').eval(env) == [ 1,2,3,4 ]
    pl('(def some-list [ 1 2 ])').eval(env)
    assert pl('some-list').eval(env) == [ 1,2 ]
    assert pl('(append! some-list 3 4)').eval(env) == [ 1,2,3,4 ]
    assert pl('some-list').eval(env) == [ 1,2,3,4 ]

def test_list_pop():
    env = Env(**functions)
    pl('(def some-list [ 1 2 3 4 ])').eval(env)
    assert pl('(pop! some-list)').eval(env) == 1
    assert pl('(pop! some-list)').eval(env) == 2
    assert pl('some-list').eval(env) == [ 3,4 ]

def test_list_pop_tail():
    env = Env(**functions)
    pl('(def some-list [ 1 2 3 4 ])').eval(env)
    assert pl('(pop! some-list -1)').eval(env) == 4
    assert pl('(pop! some-list -1)').eval(env) == 3

def test_list_sort():
    env = Env(**functions)
    assert pl('(sort [ 3 1 2 4 ])').eval(env) == [ 1,2,3,4 ]

def test_list_reverse():
    env = Env(**functions)
    assert pl('(reverse [ 1 2 3 ])').eval(env) == [ 3, 2, 1 ]

def test_list_min():
    env = Env(**functions)
    assert pl('(min [ 3 1 2 4 ])').eval(env) == 1

def test_list_max():
    env = Env(**functions)
    assert pl('(max [ 3 1 2 4 ])').eval(env) == 4

def test_undefined_error():
    env = Env(**functions)
    with pytest.raises(Exception) as e:
        pl('(add foo 1)').eval(env)
    assert "foo" in str(e)

def test_magic():
    x = 123
    env = Env(**{ "magic!": lambda: x })
    assert pl('(magic!)').eval(env) == 123
    x = 456
    assert pl('(magic!)').eval(env) == 456

def test_open_ended_substr():
    env = Env(**functions)
    assert pl('(substr "foobar" 3)').eval(env) == "bar"

def test_negative_open_ended_substr():
    env = Env(**functions)
    assert pl('(substr "foobar" -2)').eval(env) == "ar"

def test_middle_substr():
    env = Env(**functions)
    assert pl('(substr "foobar" 2 4)').eval(env) == "oba"

def test_simple_test_statememt():
    env = Env(test=TR())
    pl('(test true)').eval(env)
    pl('(test false)').eval(env)
    assert env.test.run == 2
    assert len(env.test.failed) == 1

def test_test_statememt_with_only_application():
    env = Env(test=TR())
    pl('(test (and true))').eval(env)

def test_named_test_statememt_with_multiple_tests():
    env = Env(test=TR())
    pl('(test "foo" true false)').eval(env)
    assert env.test.run == 2
    assert len(env.test.failed) == 1
    assert "foo" in env.test.failed

def test_test_function_catches_errors():
    env = Env(test=TR())
    pl('(test nope)').eval(env)
    assert env.test.run == 1
    assert len(env.test.failed) == 1

def test_and_or_in_tests():
    env = Env(test=TR(), **functions)
    pl('(test "foo" (and true true))').eval(env)
    assert env.test.run == 1
    assert len(env.test.failed) == 0

def test_test_statememt_does_nothing_without_test_context():
    env = Env()
    pl('(test nope)').eval(env) # does not raise undefined error
    assert not env.test

def test_test_statememt_inside_lambda():
    env = Env(test=TR())
    pl('((result=true) ->(test "foo" result))').eval(env)
    assert env.test.run == 1
    assert not env.test.failed

def test_defined_lambdas_know_their_name():
    env = Env()
    pl('(def foo ()->42)').eval(env)
    assert env["foo"].name == "foo"

def test_lambda_can_be_stringified():
    env = Env(**functions)
    pl('(def foo ()->42)').eval(env)
    assert pl('(str foo)').eval(env) == "𝝺 foo"
    assert pl('(str ()->42)').eval(env) == "𝝺"

def test_use_needs_a_runtime():
    with pytest.raises(Exception) as e:
        pl('(use "foo")').eval(Env())
    assert "runtime" in str(e) and "foo" in str(e)

def test_string_join():
    env = Env(**functions)
    assert pl('(join "." [ "foo" "bar" ])').eval(env) == 'foo.bar'
    assert pl('(join "" [ "foo" "bar" ])').eval(env) == 'foobar'

def test_string_split():
    env = Env(**functions)
    assert pl('(split "foo bar" " ")').eval(env) == [ "foo", "bar" ]
    assert pl('(split "foo")').eval(env) == [ "foo" ]

def test_string_trim():
    env = Env(**functions)
    assert pl('(trim  "foo bar")').eval(env) == "foo bar"
    assert pl('(trim  "  foo bar\n ")').eval(env) == "foo bar"

def test_error():
    env = Env(**functions)
    ast = pl('(error "foobar")')
    with pytest.raises(Exception) as e:
        ast.eval(env)
    assert "foobar" in str(e)
    assert isinstance(e.value, YallError)

def test_catch_can_just_pass_on_error():
    env = Env(**functions)
    ast = pl('(catch (div 1 0))')
    assert not ast.eval(env)

def test_catch_evaluates_successful_expression():
    env = Env(**functions)
    ast = pl('(catch 42)')
    assert ast.eval(env) == 42

def test_catch_can_evaluate_default_lambda():
    env = Env(**functions)
    ast = pl('(catch (div 1 0) ()->42)')
    assert ast.eval(env) == 42

def test_catch_lambda_knows_error():
    env = Env(**functions)
    ast = pl('(catch (error "foobar") e -> (str "caught error " e))')
    assert ast.eval(env) == "caught error foobar"

def test_catch_may_have_a_default_value():
    env = Env(**functions)
    ast = pl('(catch (error "foobar") "too bad")')
    assert ast.eval(env) == "too bad"

def test_catch_can_use_a_referenced_function():
    env = Env(**functions)
    pl('id = x -> x').eval(env)
    assert pl('(catch (error "foo") id)').eval(env) == "foo"

def test_error_stacktrace():
    env = Env(**functions)
    for s in parse(lex("\n".join([
            "# empty line",
            "",
            '(def bar ()-> (error "nope"))',
            '(def foo ()-> (bar))',
        ]), name="test")):
        s.eval(env)
    ast = pl('(foo)')
    with pytest.raises(Exception) as e:
        ast.eval(env)
    ex = e.value
    assert len(ex.stack) == 3
    assert "error" in ex.stack[0][1]
    assert "bar" in ex.stack[1][1]
    assert "foo" in ex.stack[2][1]

def test_refs_with_dots():
    env = Env(**functions)
    pl('(def a { b:{ c:42 } })').eval(env)
    assert pl("a.b.c").eval(env) == 42

def test_private_references():
    env = Env()
    env.privates.update({ ".a": 42 })
    assert pl(".a").eval(env) == 42

def test_stackframes_are_cleaned_after_application():
    env = Env(**functions)
    pl('(def f n -> (def x n))').eval(env)
    assert pl("(f 42)").eval(env) == 42
    with pytest.raises(Exception) as e:
        pl("x").eval(env)

def test_definition_syntactic_sugar():
    env = Env(**functions)
    pl('foo = bar = 42').eval(env)
    assert pl("foo").eval(env) == 42
    assert pl("bar").eval(env) == 42

def test_apply():
    env = Env(**functions)
    assert pl('(apply add [1 2 3 4])').eval(env) == 10

def test_null():
    env = Env(**functions)
    assert pl("nil").eval(env) is None

def test_is_null():
    env = Env(**functions)
    assert pl("(nil? nil)").eval(env)
    assert not pl("(nil? false)").eval(env)
    assert not pl("(nil? 0)").eval(env)
    assert not pl("(nil? [])").eval(env)

def test_is_func():
    env = Env(**functions)
    pl("f = x -> (add 1 x)").eval(env)
    assert pl("(func? add)").eval(env)
    assert pl("(func? f)").eval(env)
    assert not pl("(func? nil)").eval(env)
    assert not pl('(func? "")').eval(env)
    assert not pl("(func? [])").eval(env)

def test_default_arguments():
    env = Env(**functions)
    pl("a = (a=42) -> a").eval(env)
    assert pl("(a)").eval(env) == 42

def test_default_expression():
    env = Env(**functions)
    pl("a = (a=[]) -> a").eval(env)
    assert pl("(a)").eval(env) == []

def test_defaults_can_reference_other_args():
    env = Env(**functions)
    pl("f1 = (a b=a) -> [ a b ]").eval(env)
    assert pl("(f1 42)").eval(env) == [ 42, 42 ]
    pl("f2 = (a b=(add a 1) c=(add a b)) -> [ a b c ]").eval(env)
    assert pl("(f2 1)").eval(env) == [ 1, 2, 3 ]

def test_functions_dont_reuse_globals_as_params():
    env = Env(**functions)
    pl('(def n 42)').eval(env)
    pl('(def f n -> n)').eval(env)
    assert pl('(f nil)').eval(env) is None

def test_explicit_nil_arg_should_be_default_instead():
    env = Env(**functions)
    pl("f = (a=42) -> a").eval(env)
    assert pl("(f nil)").eval(env) == 42

def test_char():
    env = Env(**functions)
    assert pl("(char 97)").eval(env) == "a"

def test_ascii():
    env = Env(**functions)
    assert pl('(ascii "a")').eval(env) == 97

def test_percent_lambdas():
    env = Env(**functions)
    assert pl('((sub % 1) 43)').eval(env) == 42
    assert pl('(map (str % "!") (seq 2))').eval(env) == [ "1!", "2!" ]

def test_numbered_percent_lambdas():
    env = Env(**functions)
    assert pl('((sub %2 %) 1 43)').eval(env) == 42
    assert pl('((sub %2 %1 %3) 2 45 1)').eval(env) == 42

def test_do():
    env = Env(**functions)
    assert pl('(do 1 (sub 5 3) (add 2 1) 4)').eval(env) == 4
    with pytest.raises(Exception) as e:
        pl('(do (error) true)').eval(env)

def test_bin_and_hex_numbers():
    assert pl('0b1001001').eval(Env()) == 73
    assert pl('0x42').eval(Env()) == 66
    assert pl('0xfF').eval(Env()) == 255
    assert pl('0o42').eval(Env()) == 34

def test_get_of_datetime():
    env = Env(**functions)
    env.update({ "date": datetime(2024,12,3,4,56,7,8) })
    assert pl('(get date "year")').eval(env) == 2024
    assert pl('(get date "month")').eval(env) == 12
    assert pl('(get date "day")').eval(env) == 3
    assert pl('(get date "hour")').eval(env) == 4
    assert pl('(get date "minute")').eval(env) == 56
    assert pl('(get date "second")').eval(env) == 7

def test_not_sugar():
    env = Env(**functions)
    assert pl("! false").eval(env)
    assert not pl("! true").eval(env)
    assert pl("!(()-> 0)").eval(env)
    assert not pl("!(()-> 1)").eval(env)

def test_tuple_lambda_syntax():
    env = Env(**functions)
    pl("f = ([a b]) -> (add a b)").eval(env)
    assert pl("(f [1 2])").eval(env) == 3

def test_tuple_lambda_with_non_expanded_defaults():
    env = Env(**functions)
    pl("f = ([a b] c=3) -> (add a b c)").eval(env)
    assert pl("(f [1 2])").eval(env) == 6

def test_threading_macro():
    env = Env(**functions)
    assert pl("(-> 1 (add 2) (sub 5))").eval(env) == 2

def test_threading_macro_positional():
    env = Env(**functions)
    assert pl("(-> 1 (add 2) (sub 1 %))").eval(env) == -2

def test_named_args_in_application():
    env = Env(**functions)
    pl("f = (a b=2 c=3) -> [a b c]").eval(env)
    assert pl("(f 1)").eval(env) == [1, 2, 3]
    assert pl("(f 1 c=4)").eval(env) == [1, 2, 4]

def test_infix_expressions():
    env = Env(**functions)
    assert pl("17 `mod` 10").eval(env) == 7

def test_binary_infix_expressions():
    env = Env(**functions)
    assert pl("2 + 4").eval(env) == 6
    assert pl("2 + 4 + 2").eval(env) == 8
    assert pl("1 + 2 * 3").eval(env) == 7
    assert pl("1 * 2 + 3").eval(env) == 5
    assert pl("2 - 1").eval(env) == 1
    assert pl("1 + 2 * 3 - 4 / 2").eval(env) == (1 + 2 * 3 - 4 / 2)
    assert pl("1 + 2 * 3 > 1 * 2 + 3").eval(env) is True
    assert pl("1 + 2 * 3 < 1 * 2 + 3").eval(env) is False

def test_multiarg_lambdas():
    env = Env(**functions)
    pl("f = (a...) -> (len a)").eval(env)
    assert pl("(f 1 2 42)").eval(env) == 3
    assert pl("(f)").eval(env) == 0

def test_partial_application_on_normal_yall_lambda():
    env = Env(**functions)
    pl("f = (a b c) -> [a b c]").eval(env)
    assert callable(pl("(f 1)").eval(env))
    assert callable(pl("((f 1) 2)").eval(env))
    assert pl("((f 1) 2 3)").eval(env) == [1, 2, 3]
    assert pl("(((f 1) 2) 3)").eval(env) == [1, 2, 3]

def test_partial_application_does_not_evaluate_defaults():
    env = Env(**functions)
    pl("f = (a b c=(error)) -> [a b c]").eval(env)
    assert callable(pl("(f 1)").eval(env))
    assert pl("((f 1) 2 3)").eval(env) == [1, 2, 3]

def test_partial_clojure_evaluates_defaults():
    env = Env(**functions)
    pl("f = (a b c=(add a 42)) -> [a b c]").eval(env)
    assert callable(pl("(f 1)").eval(env))
    assert pl("((f 1) 2)").eval(env) == [1, 2, 43]

def test_partial_application_of_multiarg_lambda():
    env = Env(**functions)
    pl("f = (a b c...) -> c").eval(env)
    assert pl("((f 1) 2 1 2 1)").eval(env) == [1, 2, 1]

def test_empty_partial_application_returns_same_function():
    env = Env(**functions)
    pl("f = a -> a").eval(env)
    partial = pl("(f)").eval(env)
    assert partial is env["f"]

def test_composed_functions():
    env = Env(**functions)
    pl("inc = (add % 1)").eval(env)
    pl("dec = (sub % 1)").eval(env)
    assert pl("(inc..dec 42)").eval(env) == 42

def test_composition_of_expressions():
    env = Env(**functions)
    assert pl("((+ 1)..(+ -2) 43)").eval(env) == 42

def test_binary_math_operators():
    env = Env(**functions)
    assert pl("(+ 1 2)").eval(env) == 3
    assert pl("(- 1 2)").eval(env) == -1
    assert pl("(* 1 2)").eval(env) == 2
    assert pl("(/ 1 2)").eval(env) == 0.5

def test_partial_application_of_stdlib_functions():
    env = Env(**functions)
    assert callable(pl("(+ 1)").eval(env))
    assert pl("((+ 1) 2)").eval(env) == 3

def test_tailcall_native_references():
    env = Env(**functions)
    pl("fact = (n r=1) -> (if (eq n 0) r (fact (- n 1) (mul r n)))").eval(env)
    assert pl("(fact 3)").eval(env) == 6
    assert pl("(map fact (seq 5))").eval(env) == [1,2,6,24,120]

def test_binary_relational_operators():
    env = Env(**functions)
    assert pl("(== 1 1)").eval(env)
    assert not pl("(== 2 1)").eval(env)
    assert pl("(!= 2 1)").eval(env)
    assert not pl("(!= 1 1)").eval(env)
    assert pl("(> 2 1)").eval(env)
    assert not pl("(> 1 1)").eval(env)
    assert pl("(< 1 2)").eval(env)
    assert not pl("(< 1 1)").eval(env)
    assert pl("(< 1 2)").eval(env)
    assert pl("(<= 1 2)").eval(env)
    assert pl("(<= 1 1)").eval(env)
    assert not pl("(<= 1 0)").eval(env)
    assert pl("(>= 2 1)").eval(env)
    assert pl("(>= 1 1)").eval(env)
    assert not pl("(>= 1 2)").eval(env)
    assert pl("((== 42) 42)").eval(env)

def test_quote():
    env = Env(**functions)
    node = pl("(quote 1)").eval(env)
    assert isinstance(node, Node)
    node = pl("(quote (+ 1 2))").eval(env)
    assert isinstance(node, Node)

def test_eval():
    env = Env(**functions)
    assert pl("(eval (quote 1))").eval(env) == 1
    assert pl("(eval (quote (+ 1 2)))").eval(env) == 3

def test_describe():
    env = Env(**functions)
    assert pl("(describe 42)").eval(env) == "42"
    assert pl("(describe (+ a 1))").eval(env) == "(+ a 1)"
    pl("f = x -> (+ x 1)").eval(env)
    assert pl("(describe f)").eval(env) == "(def f (lambda (x) (+ x 1)))"

def test_cond():
    env = Env(**functions)
    pl(
        'f = (x) ->' + \
        '    (cond' + \
        '      ((== x 1) "one")' + \
        '      ((== x 2) "two")' + \
        '      (true "other"))'
    ).eval(env)
    assert pl("(f 1)").eval(env) == "one"
    assert pl("(f 2)").eval(env) == "two"
    assert pl("(f 3)").eval(env) == "other"

def test_infinite_tailcall():
    env = Env(**functions)
    pl('f = (a) -> (f a)').eval(env)
    with pytest.raises(Exception) as e:
        pl("(f 1)").eval(env)
    assert "infinite" in str(e)

def test_partial_tailcalls():
    env = Env(**functions)
    pl('f = (l a) -> (if !l l (f (cdr l) a))').eval(env)
    pl("f1 = (f [1 2])").eval(env)
    assert pl("(f1 42)").eval(env) == []

def test_new_lambda_without_args():
    env = Env(**functions)
    pl("f = () -> 42").eval(env)
    assert pl("(f)").eval(env) == 42

def test_tolist_on_string():
    env = Env(**functions)
    assert pl('(tolist "foo")').eval(env) == ["f", "o", "o"]

def test_set_can_change_variable():
    env = Env(**functions)
    pl('a = 1').eval(env)
    assert pl('(set! a 42)').eval(env) == 42
    assert pl('a').eval(env) == 42

def test_set_only_changes_variable_in_nearest_scope():
    env = Env(**functions)
    pl('a = 1').eval(env)
    pl('f = (a) -> (do (set! a 42) a)').eval(env)
    assert pl('(f 2)').eval(env) == 42
    assert pl('a').eval(env) == 1

def test_apply_can_be_partially_applied():
    env = Env(**functions)
    pl('f = (apply str)').eval(env)
    assert pl('(f ["a" "b" "c"])').eval(env) == "abc"
