from yall.lang.runtime import Runtime
import pytest
import pathlib
import os

EXAMPLES_PATH = pathlib.Path((__file__)).parent / "yall"

the_rt = Runtime()

def test_runtime():
    res = the_rt.eval("42")
    assert next(the_rt.eval("42")) == 42

def test_default_env_has_magic_and_functions():
    the_rt.eval("\n".join([
        "(env!)",
        "(min [ 1 2 3 ])",
    ]))

def test_runtime_with_multiple_statement():
    res = list(the_rt.eval("\n".join([
        "(def foo 30)",
        "(def bar (add foo 10))",
        "(add bar 2)",
    ])))
    assert len(res) ==3
    assert res[2] == 42

def test_use_file_does_not_mess_with_parent_env():
    rt = the_rt.child()
    next(rt.eval("(def foo 123)"))
    rt.use_file(EXAMPLES_PATH / "inc-dec.yall")
    assert "Inc" not in rt.env
    assert "Inc" in rt.env["inc-dec"]
    assert "foo" in rt.env
    assert "foo" not in rt.env["inc-dec"]

def test_use_file():
    rt = the_rt.child()
    rt.use_file(EXAMPLES_PATH / "inc-dec.yall")
    assert next(rt.eval("(inc-dec.Inc 41)")) == 42
    assert next(rt.eval("(inc-dec.Dec 43)")) == 42

def test_use_needs_a_runtime():
    rt = the_rt.child()
    next(rt.eval(f'(use "{EXAMPLES_PATH / "inc-dec"}")'))
    assert next(rt.eval(f'(inc-dec.Inc 0)')) == 1

def test_use_prepends_file_path_for_relative_imports():
    rt = the_rt.child()
    next(rt.eval(f'(use "{EXAMPLES_PATH / "import"}" Inc)'))
    assert next(rt.eval(f'(Inc 0)')) == 1

def test_use_considers_yall_path():
    prev_path = os.environ.get("YALLPATH", "")
    os.environ["YALLPATH"] = "/tmp:" + str(EXAMPLES_PATH)
    rt = the_rt.child()
    next(rt.eval(f'(use inc-dec)'))
    assert next(rt.eval(f'(inc-dec.Inc 0)')) == 1

def test_use_can_import_some_definitions_in_global_scope():
    rt = the_rt.child()
    result = next(rt.eval(f'(use "{EXAMPLES_PATH / "inc-dec"}" "Inc" Dec)'))
    assert "Inc" in result
    assert next(rt.eval(f'(Inc 1)')) == 2
    assert next(rt.eval(f'(Dec 3)')) == 2
    assert "Inc" in rt.env
    assert "Dec" in rt.env
    assert "inc-dec" not in rt.env

def test_runtime_can_access_yall_scope():
    rt = the_rt.child()
    result = next(rt.eval(f'test = 42'))
    assert rt.get("test") == 42
    assert rt.get("nope", 123) == 123
    with pytest.raises(Exception) as e:
        rt.get("nope")

def test_runtime_knows_all_defined_names():
    rt = the_rt.child()
    result = next(rt.eval(f'test = 42'))
    names = rt.env.names()
    assert "add" in names
    assert "system!" in names
    assert names[-1] == "test"

def test_env_can_be_altered_via_runtime():
    rt = the_rt.child()
    result = next(rt.eval(f'test = 42'))
    assert next(rt.eval("test")) == 42
    rt.set("test", 24)
    assert next(rt.eval("test")) == 24

def test_env_can_be_read_via_private():
    rt = the_rt.child()
    rt.set("foo", 42)
    assert next(rt.eval(".env"))
    assert "foo" in next(rt.eval(".env"))
    assert next(rt.eval('(has? .env "foo")')) is True

def test_parent_can_be_read_via_private():
    rt = the_rt.child()
    assert next(rt.eval('(has? .parent "add")')) is True

def test_filename_and_name_empty():
    rt = the_rt.child()
    assert not next(rt.eval('.file'))
    assert not next(rt.eval('.name'))

def test_imported_modules_know_filename_and_name():
    rt = the_rt.child()
    rt.use_file("env")
    assert next(rt.eval(f'(env.filename)')).endswith("test/yall/env.yall")
    assert next(rt.eval(f'(env.pretty-name)')) == "env"
    assert next(rt.eval(f'(env.second.filename)')) \
            .endswith("test/yall/second-env.yall")
    assert next(rt.eval(f'(env.second.pretty-name)')) == "second-env"

def test_imported_modules_knows_if_it_is_tested():
    rt = Runtime(native_stdlib=False)
    rt.use_file("env", run_tests=True)
    assert next(rt.eval(f'(env.running-tests?)'))
    assert next(rt.eval(f'(env.second.running-tests?)'))
    rt = Runtime(native_stdlib=False)
    rt.use_file("env")
    assert not next(rt.eval(f'(env.running-tests?)'))
    assert not next(rt.eval(f'(env.second.running-tests?)'))
