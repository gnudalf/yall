from yall.lang.lexer import lex, T, Token, CharStream, Pos
import pytest

def test_tokens():
    t = Token(T.STRING, "foobar")
    assert t.match(T.ID, T.STRING)
    assert not t.match(T.ID)
    assert t.value == "foobar"

def test_tokens_with_positions():
    t = Token(T.STRING, "foobar", Pos("foo", 1, 2))
    assert str(t.pos) == "foo: 1:2"

def test_nothing():
    assert lex() == []

def test_token_types():
    assert T.OPEN_PAREN == "OPEN_PAREN"
    assert T.CLOSE_PAREN == "CLOSE_PAREN"

def test_parens():
    tokens = lex("()")
    assert len(tokens) == 2
    assert not tokens[0].match(T.CLOSE_PAREN)
    assert tokens[0].match(T.OPEN_PAREN)
    assert tokens[0].value == "("
    assert not tokens[1].match(T.OPEN_PAREN)
    assert tokens[1].match(T.CLOSE_PAREN)
    assert tokens[1].value == ")"

def test_simple_id():
    tokens = lex("foo")
    assert len(tokens) == 1
    assert tokens[0].match(T.ID)
    assert tokens[0].value == "foo"

def test_full_id():
    tokens = lex("_a_BCd-123-foo!?")
    assert len(tokens) == 1
    assert tokens[0].match(T.ID)
    assert tokens[0].value == "_a_BCd-123-foo!?"

def test_integers():
    tokens = lex("123")
    assert len(tokens) == 1
    assert tokens[0].match(T.NUMBER)
    assert tokens[0].value == "123"

def test_float():
    tokens = lex("0.5")
    assert len(tokens) == 1
    assert tokens[0].match(T.NUMBER)
    assert tokens[0].value == "0.5"

def test_negative_numbers():
    tokens = lex("-42")
    assert len(tokens) == 1
    assert tokens[0].match(T.NUMBER)
    assert tokens[0].value == "-42"

def test_whitespace():
    tokens = lex("(add 1 2 3)")
    assert len(tokens) == 6
    assert tokens[0].match(T.OPEN_PAREN)
    assert tokens[1].match(T.ID)
    assert tokens[2].match(T.NUMBER)
    assert tokens[3].match(T.NUMBER)
    assert tokens[4].match(T.NUMBER)
    assert tokens[5].match(T.CLOSE_PAREN)

def test_commas_are_whitespace_too():
    tokens = lex("(add 1,2,3)")
    assert len(tokens) == 6
    assert tokens[0].match(T.OPEN_PAREN)
    assert tokens[1].match(T.ID)
    assert tokens[2].match(T.NUMBER)
    assert tokens[3].match(T.NUMBER)
    assert tokens[4].match(T.NUMBER)
    assert tokens[5].match(T.CLOSE_PAREN)

def test_lots_of_braces():
    tokens = lex("((()())())()")
    assert len(tokens) == 12
    assert tokens[0].match(T.OPEN_PAREN)
    assert tokens[11].match(T.CLOSE_PAREN)

def test_newline():
    tokens = lex("abc\ndef")
    assert len(tokens) == 2
    assert tokens[0].match(T.ID)
    assert tokens[1].match(T.ID)

def test_unknown_chars():
    with pytest.raises(Exception) as e:
        lex("€")
    assert "€" in str(e)

def test_boolean_constants():
    tokens = lex("true false")
    assert len(tokens) == 2
    assert tokens[0].match(T.BOOL)
    assert tokens[1].match(T.BOOL)

def test_comments():
    tokens = lex("123 # foo")
    assert len(tokens) == 1
    tokens = lex("123 # foo\n456")
    assert len(tokens) == 2

def test_strings():
    tokens = lex('"foo bar" ""')
    assert len(tokens) == 2
    assert tokens[0].match(T.STRING)
    assert tokens[0].value == "foo bar"
    assert tokens[1].value == ""

def test_strings_with_quotes():
    tokens = lex('"foo \\"bar\\""')
    assert len(tokens) == 1
    assert tokens[0].match(T.STRING)
    assert tokens[0].value == 'foo "bar"'

def test_strings_with_newline():
    tokens = lex('"foo\\nbar"')
    assert tokens[0].match(T.STRING)
    assert tokens[0].value == "foo\nbar"

def test_strings_with_only_newline():
    tokens = lex('"\\n"')
    assert tokens[0].match(T.STRING)
    assert tokens[0].value == "\n"

def test_lists_with_square_brackets():
    tokens = lex("[ foo 2 3 ]")
    assert len(tokens) == 5
    assert tokens[0].match(T.SQUARE_BR_OPEN)
    assert tokens[1].match(T.ID)
    assert tokens[2].match(T.NUMBER)
    assert tokens[3].match(T.NUMBER)
    assert tokens[4].match(T.SQUARE_BR_CLOSE)

def test_objects_with_square_brackets():
    tokens = lex('[ "foo"=123 bar=456 ]')
    assert len(tokens) == 8
    assert tokens[0].match(T.SQUARE_BR_OPEN)
    assert tokens[1].match(T.STRING)
    assert tokens[2].match(T.ASSIGN)
    assert tokens[3].match(T.NUMBER)
    assert tokens[4].match(T.ID)
    assert tokens[5].match(T.ASSIGN)
    assert tokens[6].match(T.NUMBER)
    assert tokens[7].match(T.SQUARE_BR_CLOSE)

def test_lambda_with_slash():
    tokens = lex('/(a b): (add a b)')
    assert len(tokens) == 11
    assert tokens[0].matchv(T.BINARY_OP, "/")
    assert tokens[1].match(T.OPEN_PAREN)
    assert tokens[2].match(T.ID)
    assert tokens[3].match(T.ID)
    assert tokens[4].match(T.CLOSE_PAREN)
    assert tokens[5].match(T.COLON)
    assert tokens[6].match(T.OPEN_PAREN)
    assert tokens[7].match(T.ID)
    assert tokens[8].match(T.ID)
    assert tokens[9].match(T.ID)
    assert tokens[10].match(T.CLOSE_PAREN)

def test_charstream():
    stream = CharStream("foo")
    assert len(stream) == 3
    assert stream.pop() == "f"
    assert stream.pop() == "o"
    assert stream.pop() == "o"
    with pytest.raises(IndexError):
        stream.pop()

def test_empty_position():
    p = Pos()
    assert p.name == ""
    assert p.line == 1
    assert p.col == 1
    assert str(p) == "1:1"

def test_position_values():
    p = Pos("foo.yall", 4, 2)
    assert p.name == "foo.yall"
    assert p.line == 4
    assert p.col == 2
    assert str(p) == "foo.yall: 4:2"
    assert p.lc() == "4:2"

def test_charstream_can_calculate_position():
    s = CharStream("foo")
    assert s.pop()
    s.remember_position()
    assert s.pos().lc() == "1:1"
    assert s.pop()
    assert s.pop()
    s.remember_position()
    assert s.pos().lc() == "1:3"

def test_charstream_can_calculate_position_after_newlines():
    s = CharStream("a\nfoo\nc")
    assert s.pop()
    s.remember_position()
    assert s.pos().lc() == "1:1"
    assert s.pop() # nl
    assert s.pop() # f
    s.remember_position()
    assert s.pos().lc() == "2:1"
    assert s.pop() # o
    assert s.pop() # o
    s.remember_position()
    assert s.pos().lc() == "2:3"
    assert s.pop() # nl
    assert s.pop() # c
    s.remember_position()
    assert s.pos().lc() == "3:1"

def test_lexed_tokens_include_position():
    tokens = lex('"foo":(\n:\n )')
    assert len(tokens) == 5
    assert tokens[0].pos.lc() == "1:1"
    assert tokens[1].pos.lc() == "1:6"
    assert tokens[2].pos.lc() == "1:7"
    assert tokens[3].pos.lc() == "2:1"
    assert tokens[4].pos.lc() == "3:2"

def test_word_tokens_include_position():
    tokens = lex('true\nfalse\n  123.0\nfoo')
    assert len(tokens) == 4
    assert tokens[0].pos.lc() == "1:1"
    assert tokens[1].pos.lc() == "2:1"
    assert tokens[2].pos.lc() == "3:3"
    assert tokens[3].pos.lc() == "4:1"

def test_lexed_tokens_include_name():
    tokens = lex("foo", name="test")
    assert len(tokens) == 1
    assert tokens[0].pos.lc() == "1:1"
    assert tokens[0].pos.name == "test"

def test_nil():
    tokens = lex("nil")
    assert len(tokens) == 1
    assert tokens[0].match(T.NIL)

def test_numbers_may_include_underscores():
    tokens = lex("1_000_000")
    assert len(tokens) == 1
    assert tokens[0].match(T.NUMBER)

def test_percent():
    tokens = lex("%")
    assert len(tokens) == 1
    assert tokens[0].match(T.PERCENT)

def test_percent_with_numbers():
    tokens = lex("%1")
    assert len(tokens) == 1
    assert tokens[0].match(T.PERCENT)

def test_underscore_is_id():
    tokens = lex("_")
    assert len(tokens) == 1
    assert tokens[0].match(T.ID)

def test_bin_and_hex_numbers():
    tokens = lex("0b1001001 0x42 0o42")
    assert len(tokens) == 3
    assert tokens[0].match(T.NUMBER)
    assert tokens[1].match(T.NUMBER)
    assert tokens[2].match(T.NUMBER)

def test_curly_braces():
    tokens = lex("{foo}")
    assert len(tokens) == 3
    assert tokens[0].match(T.CURLY_BR_OPEN)
    assert tokens[1].match(T.ID)
    assert tokens[2].match(T.CURLY_BR_CLOSE)

def test_bang_not():
    tokens = lex("!")
    assert len(tokens) == 1
    assert tokens[0].match(T.NOT)

def test_bang_in_front_of_id():
    tokens = lex("!a")
    assert len(tokens) == 2
    assert tokens[0].match(T.NOT)
    assert tokens[1].match(T.ID)

def test_thread():
    tokens = lex("->")
    assert len(tokens) == 1
    assert tokens[0].match(T.THREAD)

def test_dot():
    tokens = lex(".")
    assert len(tokens) == 1
    assert tokens[0].match(T.DOT)

def test_dot_in_word():
    tokens = lex("a.b")
    assert len(tokens) == 3
    assert tokens[0].match(T.ID)
    assert tokens[1].match(T.DOT)
    assert tokens[2].match(T.ID)

def test_dot_at_start_of_word():
    tokens = lex(".abc")
    assert len(tokens) == 1
    assert tokens[0].match(T.ID)

def test_backticks():
    tokens = lex("`foo`")
    assert len(tokens) == 3
    assert tokens[0].match(T.BACKTICK)
    assert tokens[1].match(T.ID)
    assert tokens[2].match(T.BACKTICK)

def test_double_dots():
    tokens = lex("..")
    assert len(tokens) == 1
    assert tokens[0].match(T.DOUBLE_DOT)
    tokens = lex("foo..bar")
    assert len(tokens) == 3
    assert tokens[0].match(T.ID)
    assert tokens[0].value=="foo"
    assert tokens[1].match(T.DOUBLE_DOT)
    assert tokens[2].match(T.ID)
    assert tokens[2].value=="bar"

def test_multi_dots():
    tokens = lex("...")
    assert len(tokens) == 1
    assert tokens[0].match(T.MULTI_DOT)

def test_binary_math_operators():
    tokens = lex("- + * /")
    assert len(tokens) == 4
    assert tokens[0].match(T.BINARY_OP)
    assert tokens[1].match(T.BINARY_OP)
    assert tokens[2].match(T.BINARY_OP)
    assert tokens[3].match(T.BINARY_OP)

def test_relational_binary_operators():
    tokens = lex(" == != < > <= >= ")
    assert len(tokens) == 6
    assert tokens[0].match(T.BINARY_OP)
    assert tokens[1].match(T.BINARY_OP)
    assert tokens[2].match(T.BINARY_OP)
    assert tokens[3].match(T.BINARY_OP)
    assert tokens[4].match(T.BINARY_OP)
    assert tokens[5].match(T.BINARY_OP)
