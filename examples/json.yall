#!/usr/bin/env -S yall test

# only intended as example of how a parser could be implemented
#
# magics json-encode! and json-decode! should be used instead
#
# parse function cannot decode strings of arbitrary length,
# as addons.condv breaks tailcall optimization

(use addons condv else)
(use strings surround replace NL)

################################################################################
# json encoding

json-encode = x ->
    (condv x [
        [ number? str ]
        [ bool? str ]
        [ str?    quote-str ]
        [ list?   json-encode-list ]
        [ obj?    json-encode-obj ]
        [ nil?    "null" ]
        [ else    ()-> (error (str "cannot json-encode "x)) ]
    ])

json-encode-list = l ->
    (str "["(join ", " (map json-encode l))"]")

json-key-value-pair = ([a b]) ->
    (str (quote-str a)": " (json-encode b))

json-encode-obj = o ->
    (str "{" (join ", " (map json-key-value-pair o)) "}")

quote-str = s ->
    (surround (replace s "\"" "\\\"") "\"")

(test "json encoding"
    (in? (catch (json-encode json-encode) id) "json-encode")
    (eq (json-encode 123) "123")
    (eq (json-encode 0.123) "0.123")
    (eq (json-encode true) "true")
    (eq (json-encode false) "false")
    (eq (json-encode nil) "null")
    (eq (json-encode "foo") "\"foo\"")
    (eq (json-encode "a\"b") "\"a\\\"b\"")
    (eq (json-encode []) "[]")
    (eq (json-encode [ 1 2 "a" ]) "[1, 2, \"a\"]")
    (eq (json-encode (obj)) "{}")
    (eq
        (json-encode { a:1 b:2 c:[ 1 2 3 ] })
        "{\"a\": 1, \"b\": 2, \"c\": [1, 2, 3]}"))


################################################################################

# json parsing

json-parse = string ->
    (parse-json-expr (chars string))

chars = tolist..iterate

iterate = s ->
    (if !s [] (conss (car s) (iterate (cdr s))))

lookahead = input ->
    (and input (car input))

and-check = (function value) ->
    (and value (function value))

numeric? = (and-check (le "0" % "9"))

letter? = (and-check (le "a" % "z"))

number-char? = c ->
    (or (numeric? c) (eq c "."))

seperators = (in? ["," " " ":" NL])

int-or-float = n ->
    (catch (int n) (float n))

advance! = (input tmp) ->
    (append! tmp (pop! input))

do-pop! = (input result) ->
    (do
        (pop! input)
        (result))

parse-json-expr = (input c=(lookahead input)) ->
    (condv c [
        [numeric? ()-> (parse-json-num input)]
        ["\""     ()-> (parse-json-str input)]
        [letter?  ()-> (parse-json-native input)]
        ["["      ()-> (parse-json-list input)]
        ["{"      ()-> (parse-json-obj input)]
    ])

parse-json-num = (input tmp=[] c=(lookahead input)) ->
    (condv c [
        [ number-char? ()-> (parse-json-num input (advance! input tmp)) ]
        [ else         ()-> (int-or-float (join "" tmp)) ]
    ])

parse-json-native = (input tmp=[] c=(lookahead input)) ->
    (condv c [
        [ letter? ()-> (parse-json-native input (advance! input tmp)) ]
        [ else    ()-> (condv (join "" tmp) [
                        [ "true"  true ]
                        [ "false" false ]
                        [ "null"  nil ]
                        [ else    v -> (error (str "not a native: " v))]
                    ]) ]
    ])

parse-json-str = (input tmp=[] started=false c=(lookahead input)) ->
    (condv c [
        # string start or end
        [ "\"" ()-> (do-pop!
                    input
                    ()->(if (or tmp started)
                        (join "" tmp)
                        (parse-json-str input tmp true))) ]

        # escaped characters
        # TODO things other than quotes could be escaped as well
        [ "\\" ()-> (do-pop!
                    input
                    ()-> (parse-json-str input (advance! input tmp))) ]

        # string content
        [ else ()-> (parse-json-str input (advance! input tmp)) ]
    ])

parse-json-list = (input tmp=[] started=false c=(lookahead input)) ->
    (condv c [
        # begining of list or new sublist
        [ "[" ()->(if (or tmp started)
                (parse-json-list input (append! tmp (parse-json-list input)))
                (do-pop! input ()->(parse-json-list input tmp true))) ]

        # end of list
        [ "]" ()-> (do-pop! input ()->tmp) ]

        # more items
        [ seperators ()-> (do-pop! input ()->(parse-json-list input tmp)) ]

        # items
        [ else ()-> (parse-json-list
                    input
                    (append! tmp (parse-json-expr input))) ]
    ])

parse-json-obj = (input tmp=(obj) key=false c=(lookahead input)) ->
    (condv c [
        # begining of object or subobject
        [ "{" ()-> (if key
                    (parse-json-obj
                        input
                        (cons! {key:: (parse-json-obj input)} tmp))
                    (do-pop! input ()->(parse-json-obj input tmp key))) ]

        # enf of object
        [ "}" ()-> (do-pop! input ()->tmp) ]

        # stuff in between
        [ seperators ()-> (do-pop! input ()->(parse-json-obj input tmp key)) ]

        # parse key first
        [ ()-> !key ()-> (parse-json-obj input tmp (parse-json-str input)) ]

        # then parse value
        [ else ()-> (parse-json-obj
                    input
                    (cons! {key:: (parse-json-expr input)} tmp)) ]
    ])

(test "json parsing"
    # numbers
    (eq (parse-json-num (chars "1")) 1)
    (eq (parse-json-num (chars "123")) 123)
    (eq (parse-json-num (chars "123.45")) 123.45)
    (eq (parse-json-expr (chars "123")) 123)
    (eq (parse-json-expr (chars "123.45")) 123.45)

    # strings
    (eq (parse-json-str (chars "\"\"")) "")
    (eq (parse-json-str (chars "\"abc\"")) "abc")
    (eq (parse-json-str (chars "\"abc\\\"def\"")) "abc\"def")
    (eq (parse-json-expr (chars "\"abc\"")) "abc")

    # native values
    (is? (parse-json-native (chars "true")) true)
    (is? (parse-json-native (chars "false")) false)
    (nil? (parse-json-native (chars "null")))
    (is? (parse-json-expr (chars "true")) true)
    (is? (parse-json-expr (chars "false")) false)
    (nil? (parse-json-expr (chars "null")))

    # lists
    (eq (parse-json-list (chars "[]")) [])
    (eq (parse-json-list (chars "[1]")) [1])
    (eq (parse-json-list (chars "[1, 2, true, \"foo\"]")) [1 2 true "foo"])
    (eq (parse-json-expr (chars "[1, [2, 3], 4]")) [1 [2 3] 4])

    # objects
    (eq (parse-json-obj (chars "{ \"foo\": 42 }")) {foo:42})
    (eq (parse-json-obj (chars "{ \"foo\": 42,\n \"bar\": 69 }"))
        {foo:42 bar:69})
    (eq (parse-json-obj (chars "{\"foo\":{\"bar\":42}}")) {foo:{bar:42}})

    # full parse example
    (eq
        (json-parse
"{
  \"obj\": { \"yes\": true },
  \"nums\": [1, 2, 3],
  \"other\": [\"foo\",true,false, null]
}")
        {
            obj: {yes: true}
            nums: [1 2 3]
            other: ["foo" true false nil]
        }))
