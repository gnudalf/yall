#!/usr/bin/env -S yall test

(use numbers pow round abs)
(use addons zip)


# list of bits of a number sorted by least significant

bits = (n acc=[]) ->
    (if !n acc
        (bits (int (div n 2)) (append! acc (mod n 2))))

bits-exact = (n count l=(bits n)) ->
    (flat [l (map 0 (seq (sub count (len l))))])

bit-number = (bits number=0 factor=1) ->
    (if !bits number
        (-> (car bits)
            (mul factor)
            (add number)
            (bit-number (cdr bits) % (mul factor 2))))

(test "number to bits to number"
    (eq (bits 0) [])
    (eq (bits 1) [1])
    (eq (bits 0b1001001) [ 1 0 0 1 0 0 1])
    (eq (bits 0b0001011) [ 1 1 0 1 ])
    (eq (bits-exact 1 1) [1])
    (eq (bits-exact 1 2) [1 0])
    (eq (bits-exact 5 1) [1 0 1 0 0])
    (eq (bits-exact 1 8) [1 0 0 0 0 0 0 0])
    (eq (bit-number []) 0)
    (eq (bit-number [ 0 ]) 0)
    (eq (bit-number [ 1 ]) 1)
    (eq (bit-number [ 1 0 0 1 0 0 1]) 0b1001001)
    (eq (bit-number [ 1 1 0 1 0 0 0]) 0b1011))


# bitwise comparison

bits-comparator = (fn a b acc=[]) ->
    (if (and !a !b) acc
        (bits-comparator
            fn
            (cdr a)
            (cdr b)
            (append! acc (fn (car-or0 a) (car-or0 b)))))

car-or0 = a -> (if a (car a) 0)

make-bits-comparator = (fn a b) ->
    (bits-comparator fn a b)

bits-and = (make-bits-comparator and)
bits-or  = (make-bits-comparator or)
bits-xor = (make-bits-comparator ne)


bin-comparator = (fn a b) ->
    (bit-number (fn (bits a) (bits b)))

bin-and = (bin-comparator bits-and)
bin-or  = (bin-comparator bits-or)
bin-xor = (bin-comparator bits-xor)

(test "bitwise and or xor"
    (eq (bits-and [0] [1]) [0])
    (eq (bits-and [1] [1]) [1])
    (eq (bits-and [1 1] [1 0]) [1 0])
    (eq (bits-and [ 1 1 1 1 1 1 1 0 ]
                  [ 1 1 0 1 ])
                  [ 1 1 0 1 0 0 0 0 ])
    (eq (bits-and [ 0 0 0 0 1 1 1 0 ]
                  [ 1 1 0 1 0 ])
                  [ 0 0 0 0 0 0 0 0 ])

    (eq (bits-or [0] [0]) [0])
    (eq (bits-or [0] [1]) [1])
    (eq (bits-or [1] [0]) [1])
    (eq (bits-or [0 1] [1 0]) [1 1])
    (eq (bits-or [ 1 0 0 0 1 1 1]
                 [ 1 1 0 1 0 0 0 1 ])
                 [ 1 1 0 1 1 1 1 1 ])

    (eq (bin-and 0b01010101 0xf) 0b00000101)
    (eq (bin-or  0b01010101 0xf) 0b01011111)
    (eq (bin-xor 0b01010101 0xf) 0b01011010))


# shifting

shift-bits = (fn num by result=num) ->
    (if !by result
        (shift-bits fn num (dec by) (int (fn result 2))))

right-shift = (shift-bits (/))
left-shift  = (shift-bits (*))

(test "bit shifting"
    (eq (right-shift 0b0101 1) 0b10)
    (eq (right-shift 0b01100111 4) 0b0110)
    (eq (left-shift 0b1 1) 0b10)
    (eq (left-shift 0b01100111 2) 0b0110011100))


# floating point

float32 = (
    bits
    _=(or (eq 32 (len bits)) (error "expected list of 32 bits"))
    sign=(between 0 1 bits)
    exponent=(between 1 9 bits)
    mantissa=(between 9 32 bits)
) ->
    (mul
        (pow -1 (car sign))
        (pow 2 (sub (bit-number (reverse exponent)) 127))
        (-> (seq 23)
            (map (* -1))
            (zip mantissa)
            (map ([bit factor]) -> (mul bit (pow 2 factor)))
            (conss 1)
            (apply add)))

close-enough = (a b) ->
    (lt (abs (sub a b)) 0.00001)

(test "floating point"
    !(catch (float32 []))
    (close-enough (float32 [
        0
        0 0 0 0 0 0 0 0
        0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
    ]) 0.0)
    (eq (float32 [
        0
        0 1 1 1 1 1 1 1
        0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
    ]) 1.0)
    (eq (float32 [
        1
        0 1 1 1 1 1 1 1
        0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
    ]) -1.0)
    (eq (float32 [
        0
        0 1 1 1 1 1 0 0
        0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
    ]) 0.15625)
    (close-enough (float32 [
        0
        1 0 0 0 0 1 0 1
        1 1 1 0 1 1 0 1 1 1 0 1 0 0 1 1 1 0 1 0 1 0 1
    ]) 123.4567))
