#!/usr/bin/env -S yall test

# collection of useful functions
# mostly a placeholder to gather and test potential standard library functions

(def condv (
    value conditions
    predicate=(caar conditions)
    result=(cadar conditions)
) ->
    (if (if (func? predicate) (predicate value) (eq predicate value))
        (if (func? result) (result value) result)
        (condv value (cdr conditions))))

((test-conditions = [
    [ 0          "zero" ]
    [ 1          "one" ]
    [ (gt % 5)   (str % " is more than 5") ]
    [ n -> true   str ]
]) ->
    (test "condv"
        (eq (condv 0  test-conditions) "zero")
        (eq (condv 1  test-conditions) "one")
        (eq (condv 4  test-conditions) "4")
        (eq (condv 10 test-conditions) "10 is more than 5")))


# else - useful as default cases for condv

else = ()-> true

(test "else"
    (else)
    (eq "one" (condv 1 [
        [ 1    "one" ]
        [ else "not one" ]
    ]))
    (eq "not one" (condv 2 [
        [ 1    "one" ]
        [ else "not one" ]
    ])))


# pair - useful abstraction for extracting key value pairs of an object
# pair turned out to be useful enough to warrant implementation in the
# form of the tuple lambda syntactic sugar

pair = (f p) -> (f (car p) (cadr p))

(test "pair"
    (eq ((pair add) [1 2]) 3)
    (eq ((pair (a b) -> a) [1 2]) 1)
    (eq ((pair (a b) -> b) [1 2]) 2)
    (eq ((pair (a b) -> [b a]) (car {1:2})) [2 1])
    (eq (apply obj (map (pair (a b) -> [a (add a b)]) { 1:2 3:4 5:6 }))
        { 1:3 3:7 5:11 }))


# flat - can be used to "flatten" or "expand" lists of lists

flat = (lists current=[] acc=[]) ->
    (if !current
        (if !lists
            acc
            (flat (cdr lists) (car lists) acc))
        (flat lists (cdr current) (append! acc (car current))))

(test "flat"
    (eq (flat []) [])
    (eq (flat [[1 2]]) [1 2])
    (eq (flat [[1 2] [3 4]]) [1 2 3 4])
    (eq (flat [[1 2] [3 4] [] [7 8]]) [1 2 3 4 7 8]))


# memoization - cache lambda results to avoid expensive recalculation

memoize-single-arg = (fn cache={}) ->
    (arg result = (or (in? cache arg) (fn arg))) ->
        (if (in? cache arg)
            (get cache arg)
            (do (cons! {arg::result} cache) result))


test-cache-fib = (obj)
test-fib = (memoize-single-arg
    n -> (if (le n 2) 1
            (add (test-fib (sub n 1)) (test-fib (sub n 2))))
    test-cache-fib)

(test "memoized fibonacci"
    (eq (len test-cache-fib) 0)
    (eq (test-fib 50) 12586269025)
    (eq (len test-cache-fib) 50))


# zip - zip 2 lists into a list of pairs

zip = (a b l=[]) ->
    (if !(and a b)
        l
        (zip (cdr a) (cdr b) (conss [ (car a) (car b)] l)))

(test "zip"
    (eq (zip [] []) [])
    (eq (zip [1] [2]) [[1 2]])
    (eq (sort (zip ["a" "b" "c"] [1 2 3])) [ ["a" 1] ["b" 2] ["c" 3] ]))


# unique - makes a list of unique values in input list

unique = (l acc=[]) ->
    (if !l acc
        (if (in? acc (car l))
            (unique (cdr l) acc)
            (unique (cdr l) (append! acc (car l)))))

(test "unique"
    (eq (unique []) [])
    (eq (sort (unique [ 1 2 1 3 1 4 2 3 1])) [1 2 3 4]))


# get value of object without undefined error

get-or-not = (o x) -> (and o x (in? o x) (get o x))

(test "get-or-not"
    (eq (get-or-not { a: 123 } "a") 123)
    !(get-or-not (obj) "foo")
    !(get-or-not { a: 123 } "")
    !(get-or-not { a: 123 } "nope"))


# time-run - times a function application. useful for benchmarking

time-run = (fn start=(now!)) ->
    (do
        (fn)
        (sub (now!) start) . total_seconds)


# ljoin - joins 2 lists

ljoin = (_ right left=(reverse _)) ->
    (if !left
        right
        (ljoin [] (conss (car left) right) (cdr left)))

(test "ljoin"
    (eq (ljoin [] [4 5 6]) [4 5 6])
    (eq (ljoin [3] [4 5 6]) [3 4 5 6])
    (eq (ljoin [1 2 3] [4 5 6]) [1 2 3 4 5 6]))


# quicksort - sorts a list of values

quicksort = (numbers pivot=(and numbers (car numbers))) ->
    (if (len numbers) `lt` 2
        numbers
        (ljoin
            (quicksort (filter (lt % pivot) (cdr numbers)))
            (conss pivot (quicksort (filter (ge % pivot) (cdr numbers))))))

(test "quicksort"
    (eq (quicksort []) [])
    (eq (quicksort [1]) [1])
    (eq (quicksort [1 2]) [1 2])
    (eq (quicksort [2 1]) [1 2])
    (eq (quicksort (seq 7)) [1 2 3 4 5 6 7])
    (eq (quicksort (reverse (seq 7))) [1 2 3 4 5 6 7])
    (eq (quicksort [ 2 1 5 7 4 3 6 ]) [1 2 3 4 5 6 7]))


# flip

flip = f -> (a b) -> (f b a)

(test "flip"
    (eq ((flip list) 1 2) [2 1])
    (eq ((flip (-))  1 2) 1))
