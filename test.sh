#!/bin/bash

DIR=$(realpath $(dirname $0))
export PYTHONPATH="$PYTHONPATH:$(realpath $(dirname $0))"
errors=0

pytest test/                                 || errors=$((errors+1))
echo
export YALLPATH="$DIR/examples:$DIR/test/yall"
python -m main test --nostd yall/stdlib.yall || errors=$((errors+1))
echo
python -m main test test/yall/*.yall         || errors=$((errors+1))
echo
python -m main test examples/*.yall          || errors=$((errors+1))

exit $errors
