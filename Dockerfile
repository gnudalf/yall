FROM python:3.12

WORKDIR /yall

COPY requirements* .

RUN pip install -r requirements.txt
RUN pip install -r requirements-test.txt

COPY yall yall
COPY examples examples
COPY test test
COPY main.py test.sh .

RUN ./test.sh

RUN pyinstaller -F -n yall main.py

ENV PATH /yall/dist:$PATH
ENV YALLPATH /yall/examples

CMD yall
