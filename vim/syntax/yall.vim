if exists("b:current_syntax")
  finish
endif

syntax match ids        '\v([a-zA-Z0-9\-_]+\??)'
syntax match magic      '\v([a-z\-_A-Z]+!)'
syntax match constants  '\v((0[xbo][0-9a-fA-F\_]+)?(-?[0-9]+[0-9_]*(\.[0-9]+)?)|true|nil|false|"([^"\\]|\\.)*")'
syntax match comments   '\v(#.*)'
syntax match inline     '\v(\%[0-9]?)'

syntax keyword specials if and or use test lambda def

" get all keywords from repl:
" (unique (sort (filter (in? ["a".."z"])..car ((concat `on` (map car)) .env .parent))))

syntax keyword stds add and append! apply argv! ascii between bool bool?
syntax keyword stds car cdr char chars concat cons cons! conss counter
syntax keyword stds date! dec div do else ends env! eq error exit!
syntax keyword stds file!-read file!-write filter flat flip float float?
syntax keyword stds foldl foldr foreach func? ge get gt has? http! id in?
syntax keyword stds inc infinite input! int is? join json!-decode
syntax keyword stds json!-encode le len list list? lt map math max member?
syntax keyword stds min mod mqtt! mul ne nil? not now! number? obj obj? on
syntax keyword stds or pop! print rand! reverse seq set! skip skip-while
syntax keyword stds sort split starts str str? sub substr system! take
syntax keyword stds take-while timer! timestamp! tolist trim unique uuid!
syntax keyword stds whitespace zero? zip

" extra keywords
syntax keyword stds catch error
syntax keyword stds caar cadr caaar cadar caddr cadddr caddddr

highlight constants  ctermfg=13
highlight comments   ctermfg=14
highlight magic      ctermfg=9
highlight specials   ctermfg=10
highlight stds       ctermfg=11
highlight inline     ctermfg=12

setlocal iskeyword+=-
setlocal iskeyword+=?
setlocal iskeyword+=!
setlocal nosmartindent autoindent

let b:current_syntax = 'yall'
